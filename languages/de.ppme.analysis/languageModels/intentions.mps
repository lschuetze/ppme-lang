<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:791d380e-439a-4163-b40a-c2379acf14c7(de.ppme.analysis.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" implicit="true" />
    <import index="6fcb" ref="r:8d9fc76a-100f-4d44-8987-0a52314ba857(de.ppme.analysis.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC" />
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
      <concept id="1140133623887" name="jetbrains.mps.lang.smodel.structure.Node_DeleteOperation" flags="nn" index="1PgB_6" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <property id="1238684351431" name="asCast" index="1BlNFB" />
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="2S6QgY" id="2breCPn4joj">
    <property role="TrG5h" value="toggleHerbieAnnotation" />
    <ref role="2ZfgGC" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    <node concept="2S6ZIM" id="2breCPn4jok" role="2ZfVej">
      <node concept="3clFbS" id="2breCPn4jol" role="2VODD2">
        <node concept="3clFbF" id="2breCPn50oY" role="3cqZAp">
          <node concept="Xl_RD" id="2breCPn50_z" role="3clFbG">
            <property role="Xl_RC" value="Toggle Herbie Analysis" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="2breCPn4jop" role="2ZfgGD">
      <node concept="3clFbS" id="2breCPn4joq" role="2VODD2">
        <node concept="3cpWs8" id="2breCPncc26" role="3cqZAp">
          <node concept="3cpWsn" id="2breCPncc29" role="3cpWs9">
            <property role="TrG5h" value="root" />
            <node concept="3Tqbb2" id="2breCPncc24" role="1tU5fm">
              <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
            </node>
            <node concept="2Sf5sV" id="2breCPncc5c" role="33vP2m" />
          </node>
        </node>
        <node concept="2$JKZl" id="2breCPncc7x" role="3cqZAp">
          <node concept="3clFbS" id="2breCPncc7z" role="2LFqv$">
            <node concept="3clFbF" id="2breCPnccz0" role="3cqZAp">
              <node concept="37vLTI" id="2breCPncc$u" role="3clFbG">
                <node concept="1PxgMI" id="2breCPncd53" role="37vLTx">
                  <property role="1BlNFB" value="true" />
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                  <node concept="2OqwBi" id="2breCPnccA6" role="1PxMeX">
                    <node concept="37vLTw" id="55h60OL8lbd" role="2Oq$k0">
                      <ref role="3cqZAo" node="2breCPncc29" resolve="root" />
                    </node>
                    <node concept="1mfA1w" id="2breCPnccHD" role="2OqNvi" />
                  </node>
                </node>
                <node concept="37vLTw" id="2breCPnccyZ" role="37vLTJ">
                  <ref role="3cqZAo" node="2breCPncc29" resolve="root" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2breCPnccr9" role="2$JKZa">
            <node concept="2OqwBi" id="2breCPnccbf" role="2Oq$k0">
              <node concept="37vLTw" id="4secgFfgrkP" role="2Oq$k0">
                <ref role="3cqZAo" node="2breCPncc29" resolve="root" />
              </node>
              <node concept="1mfA1w" id="2breCPncciK" role="2OqNvi" />
            </node>
            <node concept="1mIQ4w" id="2breCPnccxf" role="2OqNvi">
              <node concept="chp4Y" id="2breCPnccxE" role="cj9EA">
                <ref role="cht4Q" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="55h60OL8l4F" role="3cqZAp" />
        <node concept="3clFbJ" id="2breCPn511Z" role="3cqZAp">
          <node concept="3clFbS" id="2breCPn5120" role="3clFbx">
            <node concept="3clFbF" id="2breCPn5kXa" role="3cqZAp">
              <node concept="2OqwBi" id="2breCPn5la2" role="3clFbG">
                <node concept="2OqwBi" id="2breCPn5kYl" role="2Oq$k0">
                  <node concept="37vLTw" id="55h60OL8nNs" role="2Oq$k0">
                    <ref role="3cqZAo" node="2breCPncc29" resolve="root" />
                  </node>
                  <node concept="3CFZ6_" id="2breCPn5l5U" role="2OqNvi">
                    <node concept="3CFYIy" id="2breCPn5l6z" role="3CFYIz">
                      <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                    </node>
                  </node>
                </node>
                <node concept="1PgB_6" id="2breCPn5llc" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="2breCPn5kVH" role="3clFbw">
            <node concept="10Nm6u" id="2breCPn5kWl" role="3uHU7w" />
            <node concept="2OqwBi" id="2breCPn51EQ" role="3uHU7B">
              <node concept="37vLTw" id="55h60OL8nN7" role="2Oq$k0">
                <ref role="3cqZAo" node="2breCPncc29" resolve="root" />
              </node>
              <node concept="3CFZ6_" id="2breCPn5kRy" role="2OqNvi">
                <node concept="3CFYIy" id="2breCPn5kS3" role="3CFYIz">
                  <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="2breCPn5lm8" role="9aQIa">
            <node concept="3clFbS" id="2breCPn5lm9" role="9aQI4">
              <node concept="3clFbF" id="2breCPn5lnt" role="3cqZAp">
                <node concept="2OqwBi" id="2breCPn5lzj" role="3clFbG">
                  <node concept="2OqwBi" id="2breCPn5loC" role="2Oq$k0">
                    <node concept="37vLTw" id="55h60OL8nPr" role="2Oq$k0">
                      <ref role="3cqZAo" node="2breCPncc29" resolve="root" />
                    </node>
                    <node concept="3CFZ6_" id="2breCPn5lwd" role="2OqNvi">
                      <node concept="3CFYIy" id="2breCPn5lwQ" role="3CFYIz">
                        <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                      </node>
                    </node>
                  </node>
                  <node concept="zfrQC" id="2breCPn5lTv" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="2S6QgY" id="pw96F8ScpZ">
    <property role="TrG5h" value="toggleValueRangeAnnotation" />
    <ref role="2ZfgGC" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    <node concept="2S6ZIM" id="pw96F8Scq0" role="2ZfVej">
      <node concept="3clFbS" id="pw96F8Scq1" role="2VODD2">
        <node concept="3clFbF" id="pw96F8ScJo" role="3cqZAp">
          <node concept="Xl_RD" id="pw96F8ScJn" role="3clFbG">
            <property role="Xl_RC" value="Toggle Value Range Annotation" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2Sbjvc" id="pw96F8Se2d" role="2ZfgGD">
      <node concept="3clFbS" id="pw96F8Se2e" role="2VODD2">
        <node concept="3clFbJ" id="pw96F8Se2z" role="3cqZAp">
          <node concept="3clFbS" id="pw96F8Se2$" role="3clFbx">
            <node concept="3clFbF" id="pw96F8Se2_" role="3cqZAp">
              <node concept="2OqwBi" id="pw96F8Se2A" role="3clFbG">
                <node concept="2OqwBi" id="pw96F8Se2B" role="2Oq$k0">
                  <node concept="2Sf5sV" id="pw96F8Sseo" role="2Oq$k0" />
                  <node concept="3CFZ6_" id="pw96F8Se2D" role="2OqNvi">
                    <node concept="3CFYIy" id="pw96F8Sekh" role="3CFYIz">
                      <ref role="3CFYIx" to="6fcb:pw96F8L5EY" resolve="RangeAnnotation" />
                    </node>
                  </node>
                </node>
                <node concept="1PgB_6" id="pw96F8Se2F" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="pw96F8Se2G" role="3clFbw">
            <node concept="10Nm6u" id="pw96F8Se2H" role="3uHU7w" />
            <node concept="2OqwBi" id="pw96F8Se2I" role="3uHU7B">
              <node concept="2Sf5sV" id="pw96F8SsbI" role="2Oq$k0" />
              <node concept="3CFZ6_" id="pw96F8SegO" role="2OqNvi">
                <node concept="3CFYIy" id="pw96F8Seih" role="3CFYIz">
                  <ref role="3CFYIx" to="6fcb:pw96F8L5EY" resolve="RangeAnnotation" />
                </node>
              </node>
            </node>
          </node>
          <node concept="9aQIb" id="pw96F8Se2M" role="9aQIa">
            <node concept="3clFbS" id="pw96F8Se2N" role="9aQI4">
              <node concept="3clFbF" id="pw96F8Se2O" role="3cqZAp">
                <node concept="2OqwBi" id="pw96F8Se2P" role="3clFbG">
                  <node concept="2OqwBi" id="pw96F8Se2Q" role="2Oq$k0">
                    <node concept="2Sf5sV" id="pw96F8Ssff" role="2Oq$k0" />
                    <node concept="3CFZ6_" id="pw96F8Se2S" role="2OqNvi">
                      <node concept="3CFYIy" id="pw96F8Sem0" role="3CFYIz">
                        <ref role="3CFYIx" to="6fcb:pw96F8L5EY" resolve="RangeAnnotation" />
                      </node>
                    </node>
                  </node>
                  <node concept="zfrQC" id="pw96F8Se2U" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

