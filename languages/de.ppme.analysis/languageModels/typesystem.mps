<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0e781a3d-af8a-472f-a31b-308469037dab(de.ppme.analysis.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
    <import index="6fcb" ref="r:8d9fc76a-100f-4d44-8987-0a52314ba857(de.ppme.analysis.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534436861" name="jetbrains.mps.baseLanguage.structure.FloatType" flags="in" index="10OMs4" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="18kY7G" id="pw96F8Mvy3">
    <property role="TrG5h" value="check_RangeAnnotation" />
    <node concept="3clFbS" id="pw96F8Mvy4" role="18ibNy">
      <node concept="3cpWs8" id="pw96F8MwKO" role="3cqZAp">
        <node concept="3cpWsn" id="pw96F8MwKR" role="3cpWs9">
          <property role="TrG5h" value="lowerBound" />
          <node concept="10OMs4" id="pw96F8MwKM" role="1tU5fm" />
          <node concept="3K4zz7" id="pw96F8Mz8A" role="33vP2m">
            <node concept="2YIFZM" id="pw96F8MzuD" role="3K4E3e">
              <ref role="37wK5l" to="e2lb:~Float.parseFloat(java.lang.String):float" resolve="parseFloat" />
              <ref role="1Pybhc" to="e2lb:~Float" resolve="Float" />
              <node concept="2OqwBi" id="pw96F8MzWu" role="37wK5m">
                <node concept="2OqwBi" id="pw96F8MzxQ" role="2Oq$k0">
                  <node concept="1YBJjd" id="pw96F8Mzwh" role="2Oq$k0">
                    <ref role="1YBMHb" node="pw96F8Mvy6" resolve="rangeAnnotation" />
                  </node>
                  <node concept="3TrEf2" id="pw96F8MzL4" role="2OqNvi">
                    <ref role="3Tt5mk" to="6fcb:pw96F8L7MN" />
                  </node>
                </node>
                <node concept="3TrcHB" id="pw96F8M$99" role="2OqNvi">
                  <ref role="3TsBF5" to="tpee:gc$nh$Z" resolve="value" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="pw96F8M$bq" role="3K4GZi">
              <ref role="1PxDUh" to="e2lb:~Float" resolve="Float" />
              <ref role="3cqZAo" to="e2lb:~Float.NEGATIVE_INFINITY" resolve="NEGATIVE_INFINITY" />
            </node>
            <node concept="2OqwBi" id="pw96F8Mwpz" role="3K4Cdx">
              <node concept="2OqwBi" id="pw96F8Mw8a" role="2Oq$k0">
                <node concept="1YBJjd" id="pw96F8Mw6L" role="2Oq$k0">
                  <ref role="1YBMHb" node="pw96F8Mvy6" resolve="rangeAnnotation" />
                </node>
                <node concept="3TrEf2" id="pw96F8MwfL" role="2OqNvi">
                  <ref role="3Tt5mk" to="6fcb:pw96F8L7MN" />
                </node>
              </node>
              <node concept="3x8VRR" id="pw96F8MwJ8" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3cpWs8" id="pw96F8M$hE" role="3cqZAp">
        <node concept="3cpWsn" id="pw96F8M$hH" role="3cpWs9">
          <property role="TrG5h" value="upperBound" />
          <node concept="10OMs4" id="pw96F8M$hC" role="1tU5fm" />
          <node concept="3K4zz7" id="pw96F8M$k2" role="33vP2m">
            <node concept="2YIFZM" id="pw96F8M$k3" role="3K4E3e">
              <ref role="37wK5l" to="e2lb:~Float.parseFloat(java.lang.String):float" resolve="parseFloat" />
              <ref role="1Pybhc" to="e2lb:~Float" resolve="Float" />
              <node concept="2OqwBi" id="pw96F8M$k4" role="37wK5m">
                <node concept="2OqwBi" id="pw96F8M$k5" role="2Oq$k0">
                  <node concept="1YBJjd" id="pw96F8M$k6" role="2Oq$k0">
                    <ref role="1YBMHb" node="pw96F8Mvy6" resolve="rangeAnnotation" />
                  </node>
                  <node concept="3TrEf2" id="pw96F8M$IJ" role="2OqNvi">
                    <ref role="3Tt5mk" to="6fcb:pw96F8L7MK" />
                  </node>
                </node>
                <node concept="3TrcHB" id="pw96F8M$k8" role="2OqNvi">
                  <ref role="3TsBF5" to="tpee:gc$nh$Z" resolve="value" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="pw96F8M$k9" role="3K4GZi">
              <ref role="1PxDUh" to="e2lb:~Float" resolve="Float" />
              <ref role="3cqZAo" to="e2lb:~Float.POSITIVE_INFINITY" resolve="POSITIVE_INFINITY" />
            </node>
            <node concept="2OqwBi" id="pw96F8M$ka" role="3K4Cdx">
              <node concept="2OqwBi" id="pw96F8M$kb" role="2Oq$k0">
                <node concept="1YBJjd" id="pw96F8M$kc" role="2Oq$k0">
                  <ref role="1YBMHb" node="pw96F8Mvy6" resolve="rangeAnnotation" />
                </node>
                <node concept="3TrEf2" id="pw96F8M$_v" role="2OqNvi">
                  <ref role="3Tt5mk" to="6fcb:pw96F8L7MK" />
                </node>
              </node>
              <node concept="3x8VRR" id="pw96F8M$ke" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="pw96F8M$LZ" role="3cqZAp" />
      <node concept="3clFbJ" id="pw96F8M$VF" role="3cqZAp">
        <node concept="3clFbS" id="pw96F8M$VH" role="3clFbx">
          <node concept="2MkqsV" id="pw96F8M_rH" role="3cqZAp">
            <node concept="Xl_RD" id="pw96F8M_Kd" role="2MkJ7o">
              <property role="Xl_RC" value="Lower bound is bigger than upper bound!" />
            </node>
            <node concept="2OqwBi" id="pw96F8M_tg" role="2OEOjV">
              <node concept="1YBJjd" id="pw96F8M_rW" role="2Oq$k0">
                <ref role="1YBMHb" node="pw96F8Mvy6" resolve="rangeAnnotation" />
              </node>
              <node concept="3TrEf2" id="pw96F8M_I8" role="2OqNvi">
                <ref role="3Tt5mk" to="6fcb:pw96F8L7MN" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3eOSWO" id="pw96F8M_qC" role="3clFbw">
          <node concept="37vLTw" id="pw96F8M_qT" role="3uHU7w">
            <ref role="3cqZAo" node="pw96F8M$hH" resolve="upperBound" />
          </node>
          <node concept="37vLTw" id="pw96F8M$Z7" role="3uHU7B">
            <ref role="3cqZAo" node="pw96F8MwKR" resolve="lowerBound" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="pw96F8Mvy6" role="1YuTPh">
      <property role="TrG5h" value="rangeAnnotation" />
      <ref role="1YaFvo" to="6fcb:pw96F8L5EY" resolve="RangeAnnotation" />
    </node>
  </node>
</model>

