<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c6974c95-1acd-4f9f-b918-86c6d968c5b8(de.ppme.analysis.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="e359e0a2-368a-4c40-ae2a-e5a09f9cfd58" name="de.itemis.mps.editor.math.notations" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="6fcb" ref="r:8d9fc76a-100f-4d44-8987-0a52314ba857(de.ppme.analysis.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
      </concept>
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1149850725784" name="jetbrains.mps.lang.editor.structure.CellModel_AttributedNodeCell" flags="ng" index="2SsqMj" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1139744628335" name="jetbrains.mps.lang.editor.structure.CellModel_Image" flags="sg" stub="8104358048506731195" index="1u4HXA">
        <property id="1139746504291" name="imageFile" index="1ubRXE" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1214560368769" name="emptyNoTargetText" index="39s7Ar" />
        <property id="1139852716018" name="noTargetText" index="1$x2rV" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1198256887712" name="jetbrains.mps.lang.editor.structure.CellModel_Indent" flags="ng" index="3XFhqQ" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="e359e0a2-368a-4c40-ae2a-e5a09f9cfd58" name="de.itemis.mps.editor.math.notations">
      <concept id="5098456557379806995" name="de.itemis.mps.editor.math.notations.structure.SubscriptEditor" flags="ng" index="2zCNKE">
        <child id="5098456557379807247" name="subscript" index="2zCNcQ" />
        <child id="5098456557379807209" name="normal" index="2zCNNg" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="aOl7efy8bg">
    <ref role="1XX52x" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
    <node concept="3EZMnI" id="aOl7efya_n" role="2wV5jI">
      <node concept="l2Vlx" id="aOl7efya_o" role="2iSdaV" />
      <node concept="2SsqMj" id="aOl7efya_k" role="3EZMnx" />
      <node concept="3XFhqQ" id="aOl7efya_M" role="3EZMnx" />
      <node concept="1u4HXA" id="aOl7efya_w" role="3EZMnx">
        <property role="1ubRXE" value="${module}/resources/analysis-herbie-16.png" />
      </node>
    </node>
    <node concept="3EZMnI" id="1P5nnDyLHuC" role="6VMZX">
      <node concept="3EZMnI" id="3Sm5iJJjEao" role="3EZMnx">
        <node concept="VPM3Z" id="3Sm5iJJjEaq" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3F0ifn" id="3Sm5iJJjEas" role="3EZMnx">
          <property role="3F0ifm" value="Input Error:" />
        </node>
        <node concept="3F1sOY" id="3Sm5iJJjEaB" role="3EZMnx">
          <ref role="1NtTu8" to="6fcb:3Sm5iJJjDgy" />
        </node>
        <node concept="l2Vlx" id="3Sm5iJJjEat" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="3Sm5iJJjEaW" role="3EZMnx">
        <node concept="VPM3Z" id="3Sm5iJJjEaY" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3F0ifn" id="3Sm5iJJjEb0" role="3EZMnx">
          <property role="3F0ifm" value="Output Error:" />
        </node>
        <node concept="3F1sOY" id="3Sm5iJJjEbh" role="3EZMnx">
          <ref role="1NtTu8" to="6fcb:3Sm5iJJjDg_" />
        </node>
        <node concept="l2Vlx" id="3Sm5iJJjEb1" role="2iSdaV" />
      </node>
      <node concept="3EZMnI" id="3Sm5iJJjEbl" role="3EZMnx">
        <node concept="VPM3Z" id="3Sm5iJJjEbn" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3F0ifn" id="3Sm5iJJjEbp" role="3EZMnx">
          <property role="3F0ifm" value="Optimization:" />
        </node>
        <node concept="l2Vlx" id="3Sm5iJJjEbq" role="2iSdaV" />
        <node concept="3F1sOY" id="1P5nnDyLHuJ" role="3EZMnx">
          <ref role="1NtTu8" to="6fcb:1P5nnDyLH4t" />
        </node>
      </node>
      <node concept="2iRkQZ" id="1P5nnDyLHuF" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="pw96F8L7Nc">
    <ref role="1XX52x" to="6fcb:pw96F8L5EY" resolve="RangeAnnotation" />
    <node concept="3EZMnI" id="pw96F8RY1D" role="6VMZX">
      <node concept="2SsqMj" id="5CPlhHzq_64" role="3EZMnx" />
      <node concept="3F0ifn" id="pw96F8RY1F" role="3EZMnx">
        <property role="3F0ifm" value="∈" />
      </node>
      <node concept="l2Vlx" id="pw96F8RY1G" role="2iSdaV" />
      <node concept="3F0ifn" id="pw96F8RY1H" role="3EZMnx">
        <property role="3F0ifm" value="[" />
      </node>
      <node concept="3F1sOY" id="pw96F8RY1I" role="3EZMnx">
        <property role="1$x2rV" value="-∞" />
        <ref role="1NtTu8" to="6fcb:pw96F8L7MN" />
      </node>
      <node concept="3F0ifn" id="pw96F8RY1J" role="3EZMnx">
        <property role="3F0ifm" value=".." />
      </node>
      <node concept="3F1sOY" id="pw96F8RY1K" role="3EZMnx">
        <property role="39s7Ar" value="true" />
        <property role="1$x2rV" value="∞" />
        <ref role="1NtTu8" to="6fcb:pw96F8L7MK" />
      </node>
      <node concept="3F0ifn" id="pw96F8RY1L" role="3EZMnx">
        <property role="3F0ifm" value="]" />
      </node>
    </node>
    <node concept="2zCNKE" id="pw96F8SCXk" role="2wV5jI">
      <node concept="3F0ifn" id="pw96F8SCXE" role="2zCNcQ">
        <property role="3F0ifm" value="∈ [..]" />
      </node>
      <node concept="2SsqMj" id="pw96F8SCXA" role="2zCNNg" />
    </node>
  </node>
</model>

