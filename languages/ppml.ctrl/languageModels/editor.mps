<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:23aa484b-8573-429f-9b2b-4e36dc8db6a9(de.ppme.ctrl.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="0" />
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="lm2c" ref="r:10261a70-d383-49ff-b567-775cda9fba27(de.ppme.ctrl.structure)" implicit="true" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1142886811589" name="jetbrains.mps.lang.editor.structure.ConceptFunctionParameter_node" flags="nn" index="pncrf" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1239814640496" name="jetbrains.mps.lang.editor.structure.CellLayout_VerticalGrid" flags="nn" index="2EHx9g" />
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1225898583838" name="jetbrains.mps.lang.editor.structure.ReadOnlyModelAccessor" flags="ng" index="1HfYo3">
        <child id="1225898971709" name="getter" index="1Hhtcw" />
      </concept>
      <concept id="1225900081164" name="jetbrains.mps.lang.editor.structure.CellModel_ReadOnlyModelAccessor" flags="sg" stub="3708815482283559694" index="1HlG4h">
        <child id="1225900141900" name="modelAccessor" index="1HlULh" />
      </concept>
      <concept id="1176717841777" name="jetbrains.mps.lang.editor.structure.QueryFunction_ModelAccess_Getter" flags="in" index="3TQlhw" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="5z7tqBB6vgw">
    <ref role="1XX52x" to="lm2c:5z7tqBB6nSJ" resolve="CtrlProperty" />
    <node concept="3EZMnI" id="5z7tqBB6x_E" role="2wV5jI">
      <node concept="3F0A7n" id="5z7tqBB6x_L" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5z7tqBB8bgO" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F1sOY" id="ON9Gjni82p" role="3EZMnx">
        <ref role="1NtTu8" to="lm2c:5z7tqBB6u1p" />
      </node>
      <node concept="3EZMnI" id="3ODi6swPK$2" role="3EZMnx">
        <node concept="VPM3Z" id="3ODi6swPK$4" role="3F10Kt">
          <property role="VOm3f" value="false" />
        </node>
        <node concept="3F0ifn" id="3ODi6swPKFe" role="3EZMnx">
          <property role="3F0ifm" value=":" />
          <node concept="VechU" id="3ODi6swPRIy" role="3F10Kt">
            <property role="Vb096" value="lightGray" />
          </node>
          <node concept="VPM3Z" id="3ODi6swPUNZ" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
        </node>
        <node concept="1HlG4h" id="3ODi6swPIIx" role="3EZMnx">
          <node concept="1HfYo3" id="3ODi6swPIIz" role="1HlULh">
            <node concept="3TQlhw" id="3ODi6swPII_" role="1Hhtcw">
              <node concept="3clFbS" id="3ODi6swPIIB" role="2VODD2">
                <node concept="3clFbF" id="ON9GjniqTS" role="3cqZAp">
                  <node concept="2OqwBi" id="ON9Gjnis53" role="3clFbG">
                    <node concept="2OqwBi" id="ON9GjnirIP" role="2Oq$k0">
                      <node concept="2OqwBi" id="ON9Gjnir0w" role="2Oq$k0">
                        <node concept="pncrf" id="ON9GjniqT$" role="2Oq$k0" />
                        <node concept="3TrEf2" id="ON9Gjnirn3" role="2OqNvi">
                          <ref role="3Tt5mk" to="lm2c:5z7tqBB6u1p" />
                        </node>
                      </node>
                      <node concept="3JvlWi" id="ON9GjnirT$" role="2OqNvi" />
                    </node>
                    <node concept="2qgKlT" id="ON9Gjnisky" role="2OqNvi">
                      <ref role="37wK5l" to="tpcu:hEwIMiw" resolve="getPresentation" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="VechU" id="3ODi6swPMjk" role="3F10Kt">
            <property role="Vb096" value="lightGray" />
          </node>
          <node concept="VPM3Z" id="3ODi6swPThM" role="3F10Kt">
            <property role="VOm3f" value="false" />
          </node>
        </node>
        <node concept="l2Vlx" id="3ODi6swPK$7" role="2iSdaV" />
      </node>
      <node concept="2iRfu4" id="5z7tqBB849L" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5z7tqBB6xAm">
    <ref role="1XX52x" to="lm2c:5z7tqBB6nSj" resolve="Ctrl" />
    <node concept="3EZMnI" id="5z7tqBB6xAN" role="2wV5jI">
      <node concept="3F0ifn" id="1lih$04QD9" role="3EZMnx">
        <property role="3F0ifm" value="external control properties: " />
      </node>
      <node concept="3F0A7n" id="1lih$04QDj" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="1lih$04QDv" role="3EZMnx">
        <property role="3F0ifm" value="" />
        <node concept="pVoyu" id="1lih$04QDA" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="5z7tqBB6xAU" role="3EZMnx">
        <ref role="1NtTu8" to="lm2c:5z7tqBB6xAa" />
        <node concept="2EHx9g" id="5z7tqBB6xBa" role="2czzBx" />
        <node concept="pVoyu" id="1lih$04QDC" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="l2Vlx" id="5z7tqBB6xAQ" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="5mlvYxQWMvH">
    <ref role="1XX52x" to="lm2c:5mlvYxQWMvz" resolve="CtrlPropertyReference" />
    <node concept="1iCGBv" id="5mlvYxQWMvI" role="2wV5jI">
      <ref role="1NtTu8" to="lm2c:5mlvYxQWMv$" />
      <node concept="1sVBvm" id="5mlvYxQWMvJ" role="1sWHZn">
        <node concept="3F0A7n" id="5mlvYxQWMvK" role="2wV5jI">
          <property role="1Intyy" value="true" />
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2NTMEjkM$Po">
    <ref role="1XX52x" to="lm2c:2NTMEjkMn3F" resolve="CtrlPropertySpecification" />
    <node concept="3EZMnI" id="2NTMEjkMBbr" role="2wV5jI">
      <node concept="1iCGBv" id="2NTMEjkMBby" role="3EZMnx">
        <ref role="1NtTu8" to="lm2c:2NTMEjkMnla" />
        <node concept="1sVBvm" id="2NTMEjkMBb$" role="1sWHZn">
          <node concept="3F0A7n" id="2NTMEjkMBbF" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="2NTMEjl3bUg" role="3EZMnx">
        <property role="3F0ifm" value=":" />
      </node>
      <node concept="1HlG4h" id="2NTMEjl3c8C" role="3EZMnx">
        <node concept="1HfYo3" id="2NTMEjl3c8L" role="1HlULh">
          <node concept="3TQlhw" id="2NTMEjl3c8U" role="1Hhtcw">
            <node concept="3clFbS" id="2NTMEjl3c93" role="2VODD2">
              <node concept="3clFbF" id="2NTMEjl3ceo" role="3cqZAp">
                <node concept="2OqwBi" id="2NTMEjl3ddn" role="3clFbG">
                  <node concept="2OqwBi" id="2NTMEjl3cHn" role="2Oq$k0">
                    <node concept="2OqwBi" id="2NTMEjl3cio" role="2Oq$k0">
                      <node concept="pncrf" id="2NTMEjl3cen" role="2Oq$k0" />
                      <node concept="3TrEf2" id="2NTMEjl3cvA" role="2OqNvi">
                        <ref role="3Tt5mk" to="lm2c:2NTMEjkMnla" />
                      </node>
                    </node>
                    <node concept="3JvlWi" id="ON9GjniB0V" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="ON9GjniBkT" role="2OqNvi">
                    <ref role="37wK5l" to="tpcu:hEwIMiw" resolve="getPresentation" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="2NTMEjl3bU$" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F1sOY" id="2NTMEjl1vKK" role="3EZMnx">
        <ref role="1NtTu8" to="lm2c:2NTMEjl1tAv" />
      </node>
      <node concept="3F0ifn" id="2NTMEjl3bUU" role="3EZMnx">
        <property role="3F0ifm" value="&gt;=" />
      </node>
      <node concept="3F1sOY" id="2NTMEjl1vLm" role="3EZMnx">
        <ref role="1NtTu8" to="lm2c:2NTMEjl1tB1" />
      </node>
      <node concept="3F1sOY" id="2NTMEjkMBbZ" role="3EZMnx">
        <ref role="1NtTu8" to="lm2c:2NTMEjkMnme" />
      </node>
      <node concept="2iRfu4" id="2NTMEjkMBbu" role="2iSdaV" />
    </node>
  </node>
</model>

