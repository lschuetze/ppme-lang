<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:3e291f67-f816-41c2-80ce-97a5ee01c533(de.ppme.ctrl.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="-1" />
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="0" />
    <use id="d8f591ec-4d86-4af2-9f92-a9e93c803ffa" name="jetbrains.mps.lang.scopes" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="o8zo" ref="r:314576fc-3aee-4386-a0a5-a38348ac317d(jetbrains.mps.scope)" />
    <import index="fnmy" ref="r:89c0fb70-0977-4113-a076-5906f9d8630f(jetbrains.mps.baseLanguage.scopes)" />
    <import index="tpcu" ref="r:00000000-0000-4000-0000-011c89590282(jetbrains.mps.lang.core.behavior)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" />
    <import index="bkkh" ref="r:0249b848-8a19-49ef-a019-de89bf2328cf(de.ppme.expressions.behavior)" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="lm2c" ref="r:10261a70-d383-49ff-b567-775cda9fba27(de.ppme.ctrl.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz">
        <property id="5864038008284099149" name="isStatic" index="2Ki8OM" />
        <property id="1225194472832" name="isVirtual" index="13i0it" />
        <property id="1225194472834" name="isAbstract" index="13i0iv" />
        <reference id="1225194472831" name="overriddenMethod" index="13i0hy" />
      </concept>
      <concept id="1225194628440" name="jetbrains.mps.lang.behavior.structure.SuperNodeExpression" flags="nn" index="13iAh5">
        <reference id="5299096511375896640" name="superConcept" index="3eA5LN" />
      </concept>
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="d8f591ec-4d86-4af2-9f92-a9e93c803ffa" name="jetbrains.mps.lang.scopes">
      <concept id="8077936094962911282" name="jetbrains.mps.lang.scopes.structure.ParentScope" flags="nn" index="iy90A" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1180031783296" name="jetbrains.mps.lang.smodel.structure.Concept_IsSubConceptOfOperation" flags="nn" index="2Zo12i">
        <child id="1180031783297" name="conceptArgument" index="2Zo12j" />
      </concept>
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1172420572800" name="jetbrains.mps.lang.smodel.structure.SConceptType" flags="in" index="3THzug" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="13h7C7" id="3ODi6swQOec">
    <ref role="13h7C2" to="lm2c:5z7tqBB6nSj" resolve="Ctrl" />
    <node concept="13hLZK" id="3ODi6swQOed" role="13h7CW">
      <node concept="3clFbS" id="3ODi6swQOee" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="3ODi6swQOei" role="13h7CS">
      <property role="TrG5h" value="getScope" />
      <property role="2Ki8OM" value="false" />
      <ref role="13i0hy" to="tpcu:3fifI_xCJOQ" resolve="getScope" />
      <node concept="3clFbS" id="3ODi6swQOel" role="3clF47">
        <node concept="3clFbH" id="6r3vkxc0FMN" role="3cqZAp" />
        <node concept="3clFbJ" id="6r3vkxc0FXA" role="3cqZAp">
          <node concept="3clFbS" id="6r3vkxc0FXC" role="3clFbx">
            <node concept="3cpWs6" id="6r3vkxc0Ged" role="3cqZAp">
              <node concept="2YIFZM" id="6r3vkxc0Gvu" role="3cqZAk">
                <ref role="37wK5l" to="fnmy:3A2qfoxVU7a" resolve="forVariables" />
                <ref role="1Pybhc" to="fnmy:3A2qfoxVUBF" resolve="Scopes" />
                <node concept="37vLTw" id="6r3vkxc0GCA" role="37wK5m">
                  <ref role="3cqZAo" node="3ODi6swQOeu" resolve="kind" />
                </node>
                <node concept="2OqwBi" id="6r3vkxc0GSD" role="37wK5m">
                  <node concept="13iPFW" id="6r3vkxc0GLX" role="2Oq$k0" />
                  <node concept="2Rf3mk" id="6r3vkxc0HjO" role="2OqNvi">
                    <node concept="1xMEDy" id="6r3vkxc0HjQ" role="1xVPHs">
                      <node concept="chp4Y" id="6r3vkxc0HtH" role="ri$Ld">
                        <ref role="cht4Q" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="iy90A" id="6r3vkxc0HNk" role="37wK5m" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6r3vkxc0G7K" role="3clFbw">
            <node concept="37vLTw" id="6r3vkxc0G5f" role="2Oq$k0">
              <ref role="3cqZAo" node="3ODi6swQOeu" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="6r3vkxc0Gdp" role="2OqNvi">
              <node concept="chp4Y" id="6r3vkxc0GdK" role="2Zo12j">
                <ref role="cht4Q" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3ODi6swR9NX" role="3cqZAp" />
        <node concept="3clFbF" id="5o9jvTM9GZY" role="3cqZAp">
          <node concept="2OqwBi" id="5o9jvTM9GZV" role="3clFbG">
            <node concept="13iAh5" id="5o9jvTM9GZW" role="2Oq$k0">
              <ref role="3eA5LN" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
            </node>
            <node concept="2qgKlT" id="5o9jvTM9GZX" role="2OqNvi">
              <ref role="37wK5l" to="tpcu:3fifI_xCJOQ" resolve="getScope" />
              <node concept="37vLTw" id="5o9jvTM9GZT" role="37wK5m">
                <ref role="3cqZAo" node="3ODi6swQOeu" resolve="kind" />
              </node>
              <node concept="37vLTw" id="5o9jvTM9GZU" role="37wK5m">
                <ref role="3cqZAo" node="3ODi6swQOew" resolve="child" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="3ODi6swQOeu" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3THzug" id="3ODi6swQOev" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="3ODi6swQOew" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="3ODi6swQOex" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="3ODi6swQOey" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
      <node concept="3Tm1VV" id="3ODi6swQOez" role="1B3o_S" />
    </node>
  </node>
  <node concept="13h7C7" id="6r3vkxc0rK4">
    <ref role="13h7C2" to="lm2c:5z7tqBB6nSJ" resolve="CtrlProperty" />
    <node concept="13hLZK" id="6r3vkxc0rK5" role="13h7CW">
      <node concept="3clFbS" id="6r3vkxc0rK6" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="6r3vkxc0rLj" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="TrG5h" value="getScope" />
      <property role="13i0it" value="false" />
      <ref role="13i0hy" to="tpcu:3fifI_xCJOQ" resolve="getScope" />
      <node concept="3Tm1VV" id="6r3vkxc0rLk" role="1B3o_S" />
      <node concept="3clFbS" id="6r3vkxc0rLt" role="3clF47">
        <node concept="3clFbH" id="6r3vkxc0rN5" role="3cqZAp" />
        <node concept="3clFbJ" id="6r3vkxc0s53" role="3cqZAp">
          <node concept="3clFbS" id="6r3vkxc0s55" role="3clFbx">
            <node concept="3cpWs6" id="6r3vkxc0snw" role="3cqZAp">
              <node concept="2YIFZM" id="6r3vkxc0sug" role="3cqZAk">
                <ref role="37wK5l" to="fnmy:3A2qfoxXvCK" resolve="forVariables" />
                <ref role="1Pybhc" to="fnmy:3A2qfoxVUBF" resolve="Scopes" />
                <node concept="37vLTw" id="6r3vkxc0sxn" role="37wK5m">
                  <ref role="3cqZAo" node="6r3vkxc0rLu" resolve="kind" />
                </node>
                <node concept="13iPFW" id="6r3vkxc0s$E" role="37wK5m" />
                <node concept="iy90A" id="6r3vkxc0sT$" role="37wK5m" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6r3vkxc0s8V" role="3clFbw">
            <node concept="37vLTw" id="6r3vkxc0s6s" role="2Oq$k0">
              <ref role="3cqZAo" node="6r3vkxc0rLu" resolve="kind" />
            </node>
            <node concept="2Zo12i" id="6r3vkxc0sk5" role="2OqNvi">
              <node concept="chp4Y" id="6r3vkxc0smp" role="2Zo12j">
                <ref role="cht4Q" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="6r3vkxc0rNd" role="3cqZAp" />
        <node concept="3clFbF" id="6r3vkxc0rLC" role="3cqZAp">
          <node concept="2OqwBi" id="6r3vkxc0rL_" role="3clFbG">
            <node concept="13iAh5" id="6r3vkxc0rLA" role="2Oq$k0">
              <ref role="3eA5LN" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
            </node>
            <node concept="2qgKlT" id="6r3vkxc0rLB" role="2OqNvi">
              <ref role="37wK5l" to="tpcu:3fifI_xCJOQ" resolve="getScope" />
              <node concept="37vLTw" id="6r3vkxc0rLz" role="37wK5m">
                <ref role="3cqZAo" node="6r3vkxc0rLu" resolve="kind" />
              </node>
              <node concept="37vLTw" id="6r3vkxc0rL$" role="37wK5m">
                <ref role="3cqZAo" node="6r3vkxc0rLw" resolve="child" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="6r3vkxc0rLu" role="3clF46">
        <property role="TrG5h" value="kind" />
        <node concept="3THzug" id="6r3vkxc0rLv" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="6r3vkxc0rLw" role="3clF46">
        <property role="TrG5h" value="child" />
        <node concept="3Tqbb2" id="6r3vkxc0rLx" role="1tU5fm" />
      </node>
      <node concept="3uibUv" id="6r3vkxc0rLy" role="3clF45">
        <ref role="3uigEE" to="o8zo:3fifI_xCtN$" resolve="Scope" />
      </node>
    </node>
  </node>
  <node concept="13h7C7" id="pw96F8Rghi">
    <ref role="13h7C2" to="lm2c:5mlvYxQWMvz" resolve="CtrlPropertyReference" />
    <node concept="13hLZK" id="pw96F8Rghj" role="13h7CW">
      <node concept="3clFbS" id="pw96F8Rghk" role="2VODD2" />
    </node>
    <node concept="13i0hz" id="pw96F8Rghl" role="13h7CS">
      <property role="13i0iv" value="false" />
      <property role="13i0it" value="false" />
      <property role="TrG5h" value="getVariable" />
      <ref role="13i0hy" to="bkkh:5l83jlMivof" resolve="getVariable" />
      <node concept="3Tm1VV" id="pw96F8Rghm" role="1B3o_S" />
      <node concept="3clFbS" id="pw96F8Rghp" role="3clF47">
        <node concept="3cpWs6" id="pw96F8RgjQ" role="3cqZAp">
          <node concept="2OqwBi" id="pw96F8RgmT" role="3cqZAk">
            <node concept="13iPFW" id="pw96F8Rgk8" role="2Oq$k0" />
            <node concept="3TrEf2" id="pw96F8Rgz$" role="2OqNvi">
              <ref role="3Tt5mk" to="lm2c:5mlvYxQWMv$" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tqbb2" id="pw96F8Rghq" role="3clF45">
        <ref role="ehGHo" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
      </node>
    </node>
  </node>
</model>

