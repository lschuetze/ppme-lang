<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:10261a70-d383-49ff-b567-775cda9fba27(de.ppme.ctrl.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="q0gb" ref="r:4efcc790-8258-4a5c-a60f-10a5b5a367a2(de.ppme.base.structure)" implicit="true" />
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" implicit="true" />
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" implicit="true" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5z7tqBB6nSj">
    <property role="TrG5h" value="Ctrl" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="Ctrl" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="Opj2YGta5e" role="1TKVEl">
      <property role="TrG5h" value="origin" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="5z7tqBB6xAa" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="properties" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5z7tqBB6nSJ" resolve="CtrlProperty" />
    </node>
    <node concept="PrWs8" id="3ODi6swQOdO" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="PrWs8" id="1lih$04QCQ" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="5z7tqBB6nSJ">
    <property role="TrG5h" value="CtrlProperty" />
    <property role="R4oN_" value="&lt;propterty&gt; = &lt;value&gt;" />
    <ref role="1TJDcQ" to="c9eo:5l83jlMhgCt" resolve="Statement" />
    <node concept="PrWs8" id="6r3vkxc0rJC" role="PzmwI">
      <ref role="PrY4T" to="q0gb:6fgLCPsE7bT" resolve="IIdentifierNamedConcept" />
    </node>
    <node concept="PrWs8" id="6r3vkxc0acS" role="PzmwI">
      <ref role="PrY4T" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
    <node concept="PrWs8" id="6r3vkxc0rJR" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="5z7tqBB6u1p" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="value" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5mlvYxQWMvz">
    <property role="TrG5h" value="CtrlPropertyReference" />
    <ref role="1TJDcQ" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    <node concept="1TJgyj" id="5mlvYxQWMv$" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20lbJX" value="1" />
      <property role="20kJfa" value="ctrlProperty" />
      <ref role="20lvS9" node="5z7tqBB6nSJ" resolve="CtrlProperty" />
    </node>
    <node concept="PrWs8" id="pw96F8RgcI" role="PzmwI">
      <ref role="PrY4T" to="pfd6:5l83jlMhoVs" resolve="IVariableReference" />
    </node>
  </node>
  <node concept="1TIwiD" id="2NTMEjkMn3F">
    <property role="TrG5h" value="CtrlPropertySpecification" />
    <property role="34LRSv" value="property reference specification" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2NTMEjkMnla" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="property" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5z7tqBB6nSJ" resolve="CtrlProperty" />
    </node>
    <node concept="1TJgyj" id="2NTMEjkMnme" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="help_txt" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2NTMEjl1tAv" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="default" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="2NTMEjl1tB1" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="min" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
</model>

