<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:28ec4f29-ca9d-42af-a9c1-0422dfd0acb2(de.ppme.ctrl.intentions)">
  <persistence version="9" />
  <languages>
    <use id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="c3oy" ref="r:8a0fa9ef-3e8e-4313-a85b-46b3640418fd(de.ppme.modules.structure)" implicit="true" />
    <import index="srng" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.openapi.editor(MPS.Editor/jetbrains.mps.openapi.editor@java_stub)" implicit="true" />
    <import index="lm2c" ref="r:10261a70-d383-49ff-b567-775cda9fba27(de.ppme.ctrl.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1194033889146" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1XNTG" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="d7a92d38-f7db-40d0-8431-763b0c3c9f20" name="jetbrains.mps.lang.intentions">
      <concept id="1192794744107" name="jetbrains.mps.lang.intentions.structure.IntentionDeclaration" flags="ig" index="2S6QgY" />
      <concept id="1192794782375" name="jetbrains.mps.lang.intentions.structure.DescriptionBlock" flags="in" index="2S6ZIM" />
      <concept id="1192795771125" name="jetbrains.mps.lang.intentions.structure.IsApplicableBlock" flags="in" index="2SaL7w" />
      <concept id="1192795911897" name="jetbrains.mps.lang.intentions.structure.ExecuteBlock" flags="in" index="2Sbjvc" />
      <concept id="1192796902958" name="jetbrains.mps.lang.intentions.structure.ConceptFunctionParameter_node" flags="nn" index="2Sf5sV" />
      <concept id="2522969319638091381" name="jetbrains.mps.lang.intentions.structure.BaseIntentionDeclaration" flags="ig" index="2ZfUlf">
        <property id="2522969319638091386" name="isAvailableInChildNodes" index="2ZfUl0" />
        <reference id="2522969319638198290" name="forConcept" index="2ZfgGC" />
        <child id="2522969319638198291" name="executeFunction" index="2ZfgGD" />
        <child id="2522969319638093995" name="isApplicableFunction" index="2ZfVeh" />
        <child id="2522969319638093993" name="descriptionFunction" index="2ZfVej" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1227022179634" name="jetbrains.mps.baseLanguage.collections.structure.AddLastElementOperation" flags="nn" index="2Ke9KJ" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
      <concept id="1165595910856" name="jetbrains.mps.baseLanguage.collections.structure.GetLastOperation" flags="nn" index="1yVyf7" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="2S6QgY" id="2NTMEjkL8F0">
    <property role="TrG5h" value="AddPropertyReferenceSpecification" />
    <property role="2ZfUl0" value="true" />
    <ref role="2ZfgGC" to="lm2c:5mlvYxQWMvz" resolve="CtrlPropertyReference" />
    <node concept="2Sbjvc" id="2NTMEjkL8F1" role="2ZfgGD">
      <node concept="3clFbS" id="2NTMEjkL8F2" role="2VODD2">
        <node concept="3cpWs8" id="2NTMEjkQ4_9" role="3cqZAp">
          <node concept="3cpWsn" id="2NTMEjkQ4_f" role="3cpWs9">
            <property role="TrG5h" value="propertySpecification" />
            <node concept="2ShNRf" id="2NTMEjkQJso" role="33vP2m">
              <node concept="3zrR0B" id="2NTMEjkQJx4" role="2ShVmc">
                <node concept="3Tqbb2" id="2NTMEjkQJx6" role="3zrR0E">
                  <ref role="ehGHo" to="lm2c:2NTMEjkMn3F" resolve="CtrlPropertySpecification" />
                </node>
              </node>
            </node>
            <node concept="3Tqbb2" id="2NTMEjkQJ$$" role="1tU5fm">
              <ref role="ehGHo" to="lm2c:2NTMEjkMn3F" resolve="CtrlPropertySpecification" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2NTMEjkQKcK" role="3cqZAp">
          <node concept="37vLTI" id="2NTMEjkQLkU" role="3clFbG">
            <node concept="2OqwBi" id="2NTMEjkQLpN" role="37vLTx">
              <node concept="2Sf5sV" id="2NTMEjkQLnL" role="2Oq$k0" />
              <node concept="3TrEf2" id="2NTMEjkQLH2" role="2OqNvi">
                <ref role="3Tt5mk" to="lm2c:5mlvYxQWMv$" />
              </node>
            </node>
            <node concept="2OqwBi" id="2NTMEjkQKN5" role="37vLTJ">
              <node concept="37vLTw" id="2NTMEjkQKcI" role="2Oq$k0">
                <ref role="3cqZAo" node="2NTMEjkQ4_f" resolve="propertySpecification" />
              </node>
              <node concept="3TrEf2" id="2NTMEjkQKVD" role="2OqNvi">
                <ref role="3Tt5mk" to="lm2c:2NTMEjkMnla" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="2NTMEjkQ5cY" role="3cqZAp" />
        <node concept="3cpWs8" id="2NTMEjkPG6Z" role="3cqZAp">
          <node concept="3cpWsn" id="2NTMEjkPG75" role="3cpWs9">
            <property role="TrG5h" value="rootModule" />
            <node concept="2OqwBi" id="2NTMEjkPGKI" role="33vP2m">
              <node concept="2Sf5sV" id="2NTMEjkPGIN" role="2Oq$k0" />
              <node concept="2Xjw5R" id="2NTMEjkPH2V" role="2OqNvi">
                <node concept="1xMEDy" id="2NTMEjkPH2X" role="1xVPHs">
                  <node concept="chp4Y" id="2NTMEjkPH3M" role="ri$Ld">
                    <ref role="cht4Q" to="c3oy:7eZWuAL6NR2" resolve="Module" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3Tqbb2" id="2NTMEjkPHc9" role="1tU5fm">
              <ref role="ehGHo" to="c3oy:7eZWuAL6NR2" resolve="Module" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2NTMEjkP_vF" role="3cqZAp">
          <node concept="2OqwBi" id="2NTMEjkPB0I" role="3clFbG">
            <node concept="2OqwBi" id="2NTMEjkPA6o" role="2Oq$k0">
              <node concept="37vLTw" id="2NTMEjkPI1K" role="2Oq$k0">
                <ref role="3cqZAo" node="2NTMEjkPG75" resolve="rootModule" />
              </node>
              <node concept="3Tsc0h" id="2NTMEjkPAk8" role="2OqNvi">
                <ref role="3TtcxE" to="c3oy:2NTMEjkMGlj" />
              </node>
            </node>
            <node concept="2Ke9KJ" id="2NTMEjkPCaL" role="2OqNvi">
              <node concept="37vLTw" id="2NTMEjkQLKD" role="25WWJ7">
                <ref role="3cqZAo" node="2NTMEjkQ4_f" resolve="propertySpecification" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2NTMEjkPERZ" role="3cqZAp">
          <node concept="2OqwBi" id="2NTMEjkPFsI" role="3clFbG">
            <node concept="1XNTG" id="2NTMEjkPERX" role="2Oq$k0" />
            <node concept="liA8E" id="2NTMEjkPJ1a" role="2OqNvi">
              <ref role="37wK5l" to="srng:~EditorContext.select(org.jetbrains.mps.openapi.model.SNode):void" resolve="select" />
              <node concept="2OqwBi" id="2Xvn13H37pi" role="37wK5m">
                <node concept="2OqwBi" id="2NTMEjkPKoR" role="2Oq$k0">
                  <node concept="2OqwBi" id="2NTMEjkPJe1" role="2Oq$k0">
                    <node concept="37vLTw" id="2NTMEjkPJaU" role="2Oq$k0">
                      <ref role="3cqZAo" node="2NTMEjkPG75" resolve="rootModule" />
                    </node>
                    <node concept="3Tsc0h" id="2NTMEjkPJAM" role="2OqNvi">
                      <ref role="3TtcxE" to="c3oy:2NTMEjkMGlj" />
                    </node>
                  </node>
                  <node concept="1yVyf7" id="2NTMEjkPME8" role="2OqNvi" />
                </node>
                <node concept="3TrEf2" id="2Xvn13H37_F" role="2OqNvi">
                  <ref role="3Tt5mk" to="lm2c:2NTMEjl1tAv" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2S6ZIM" id="2NTMEjkL8F3" role="2ZfVej">
      <node concept="3clFbS" id="2NTMEjkL8F4" role="2VODD2">
        <node concept="3clFbF" id="2NTMEjkLMAf" role="3cqZAp">
          <node concept="Xl_RD" id="2NTMEjkLMAe" role="3clFbG">
            <property role="Xl_RC" value="Add Ctrl-property Specificaton" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2SaL7w" id="2NTMEjkPNRF" role="2ZfVeh">
      <node concept="3clFbS" id="2NTMEjkPNRG" role="2VODD2">
        <node concept="3cpWs8" id="2NTMEjkPQF3" role="3cqZAp">
          <node concept="3cpWsn" id="2NTMEjkPQF4" role="3cpWs9">
            <property role="TrG5h" value="rootModule" />
            <node concept="2OqwBi" id="2NTMEjkPQF5" role="33vP2m">
              <node concept="2Sf5sV" id="2NTMEjkPQF6" role="2Oq$k0" />
              <node concept="2Xjw5R" id="2NTMEjkPQF7" role="2OqNvi">
                <node concept="1xMEDy" id="2NTMEjkPQF8" role="1xVPHs">
                  <node concept="chp4Y" id="2NTMEjkPQF9" role="ri$Ld">
                    <ref role="cht4Q" to="c3oy:7eZWuAL6NR2" resolve="Module" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3Tqbb2" id="2NTMEjkPQFa" role="1tU5fm">
              <ref role="ehGHo" to="c3oy:7eZWuAL6NR2" resolve="Module" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="2NTMEjkPQQ3" role="3cqZAp">
          <node concept="2OqwBi" id="2NTMEjkPYIF" role="3clFbG">
            <node concept="2OqwBi" id="2NTMEjkPS3J" role="2Oq$k0">
              <node concept="2OqwBi" id="2NTMEjkPQV$" role="2Oq$k0">
                <node concept="37vLTw" id="2NTMEjkPQQ1" role="2Oq$k0">
                  <ref role="3cqZAo" node="2NTMEjkPQF4" resolve="rootModule" />
                </node>
                <node concept="3Tsc0h" id="2NTMEjkPRcY" role="2OqNvi">
                  <ref role="3TtcxE" to="c3oy:2NTMEjkMGlj" />
                </node>
              </node>
              <node concept="3zZkjj" id="2NTMEjkPWQ4" role="2OqNvi">
                <node concept="1bVj0M" id="2NTMEjkPWQ7" role="23t8la">
                  <node concept="3clFbS" id="2NTMEjkPWQ8" role="1bW5cS">
                    <node concept="3clFbF" id="2NTMEjkPX11" role="3cqZAp">
                      <node concept="3clFbC" id="2NTMEjkPXNG" role="3clFbG">
                        <node concept="2OqwBi" id="2NTMEjkPY8e" role="3uHU7w">
                          <node concept="2Sf5sV" id="2NTMEjkPXYN" role="2Oq$k0" />
                          <node concept="3TrEf2" id="2NTMEjkPYrT" role="2OqNvi">
                            <ref role="3Tt5mk" to="lm2c:5mlvYxQWMv$" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="2NTMEjkPX9B" role="3uHU7B">
                          <node concept="37vLTw" id="2NTMEjkPX10" role="2Oq$k0">
                            <ref role="3cqZAo" node="2NTMEjkPWQ9" resolve="it" />
                          </node>
                          <node concept="3TrEf2" id="2NTMEjkPXsa" role="2OqNvi">
                            <ref role="3Tt5mk" to="lm2c:2NTMEjkMnla" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="2NTMEjkPWQ9" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="2NTMEjkPWQa" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1v1jN8" id="2NTMEjkPZwU" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

