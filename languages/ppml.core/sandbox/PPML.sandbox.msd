<?xml version="1.0" encoding="UTF-8"?>
<solution name="usecases" uuid="4e182329-9671-4fc4-9956-481dbe14f7cf" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">76a86d18-59ae-4f16-b531-8543c06a850b(de.ppme.plugins)</dependency>
  </dependencies>
  <usedLanguages>
    <usedLanguage>92d2ea16-5a42-4fdf-a676-c7604efe3504(de.slisson.mps.richtext)</usedLanguage>
    <usedLanguage>13ff48ad-ecdb-4625-9d4a-3a16f2228549(de.ppme.analysis)</usedLanguage>
    <usedLanguage>f3061a53-9226-4cc5-a443-f952ceaf5816(jetbrains.mps.baseLanguage)</usedLanguage>
    <usedLanguage>83888646-71ce-4f1c-9c53-c54016f6ad4f(jetbrains.mps.baseLanguage.collections)</usedLanguage>
    <usedLanguage>cd53cec3-9093-4895-940d-de1b7abe9936(de.ppme.physunits)</usedLanguage>
    <usedLanguage>a206eff4-e667-4146-8006-8cce4ef80954(de.ppme.modules)</usedLanguage>
    <usedLanguage>f7af8208-82a7-4aab-9829-b32680223c52(de.ppme.ctrl)</usedLanguage>
    <usedLanguage>63e1a497-af15-4608-bf36-c0aa0aaf901b(de.ppme.core)</usedLanguage>
  </usedLanguages>
  <usedDevKits>
    <usedDevKit>2ddb73c1-8857-47ab-adb2-5c8e43ee1765(de.ppme.lang)</usedDevKit>
  </usedDevKits>
  <languageVersions>
    <language id="13ff48ad-ecdb-4625-9d4a-3a16f2228549" fqName="de.ppme.analysis" version="0" />
    <language id="ad43ad83-7356-4c72-888b-881fea736282" fqName="de.ppme.base" version="-1" />
    <language id="63e1a497-af15-4608-bf36-c0aa0aaf901b" fqName="de.ppme.core" version="0" />
    <language id="f7af8208-82a7-4aab-9829-b32680223c52" fqName="de.ppme.ctrl" version="0" />
    <language id="58df54a5-af81-48d5-bb5c-9db2412768a4" fqName="de.ppme.expressions" version="0" />
    <language id="a206eff4-e667-4146-8006-8cce4ef80954" fqName="de.ppme.modules" version="0" />
    <language id="cd53cec3-9093-4895-940d-de1b7abe9936" fqName="de.ppme.physunits" version="0" />
    <language id="7d7a484c-83ab-4a7c-b9f3-8672c02bd0f6" fqName="de.ppme.statements" version="0" />
    <language id="92d2ea16-5a42-4fdf-a676-c7604efe3504" fqName="de.slisson.mps.richtext" version="0" />
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" fqName="jetbrains.mps.baseLanguage" version="1" />
    <language id="ed6d7656-532c-4bc2-81d1-af945aeb8280" fqName="jetbrains.mps.baseLanguage.blTypes" version="0" />
    <language id="fd392034-7849-419d-9071-12563d152375" fqName="jetbrains.mps.baseLanguage.closures" version="0" />
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" fqName="jetbrains.mps.baseLanguage.collections" version="0" />
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" fqName="jetbrains.mps.lang.core" version="1" />
    <language id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" fqName="jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
</solution>

