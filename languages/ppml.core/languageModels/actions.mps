<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:1a7a8ccb-411f-456f-a453-818d1d85c72b(de.ppme.modules.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="c3oy" ref="r:8a0fa9ef-3e8e-4313-a85b-46b3640418fd(de.ppme.modules.structure)" implicit="true" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="1196433923911" name="jetbrains.mps.lang.actions.structure.SideTransform_SimpleString" flags="nn" index="2h1dTh">
        <property id="1196433942569" name="text" index="2h1i$Z" />
      </concept>
      <concept id="1177323996388" name="jetbrains.mps.lang.actions.structure.AddMenuPart" flags="ng" index="tYCnQ" />
      <concept id="1177333529597" name="jetbrains.mps.lang.actions.structure.ConceptPart" flags="ng" index="uyZFJ">
        <reference id="1177333551023" name="concept" index="uz4UX" />
        <child id="1177333559040" name="part" index="uz6Si" />
      </concept>
      <concept id="1177497140107" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_sourceNode" flags="nn" index="Cj7Ep" />
      <concept id="1177498013932" name="jetbrains.mps.lang.actions.structure.SimpleSideTransformMenuPart" flags="ng" index="Cmt7Y">
        <child id="1177498166690" name="matchingText" index="Cn2iK" />
        <child id="1177498207384" name="handler" index="Cncma" />
      </concept>
      <concept id="1177498227294" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_Handler" flags="in" index="Cnhdc" />
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
      <concept id="1154622616118" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstitutePreconditionFunction" flags="in" index="3kRJcU" />
      <concept id="1138079221458" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActionsBuilder" flags="ig" index="3UNGvq">
        <property id="1215605257730" name="side" index="7I3sp" />
        <property id="1140829165817" name="transformTag" index="2uHTBK" />
        <reference id="1138079221462" name="applicableConcept" index="3UNGvu" />
        <child id="1177442283389" name="part" index="_1QTJ" />
        <child id="1154622757656" name="precondition" index="3kShCk" />
      </concept>
      <concept id="1138079416598" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActions" flags="ng" index="3UOs0u">
        <child id="1138079416599" name="actionsBuilder" index="3UOs0v" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3UOs0u" id="6fgLCPsC8iF">
    <property role="3GE5qa" value="" />
    <property role="TrG5h" value="addDescription" />
    <node concept="3UNGvq" id="6fgLCPsC8iG" role="3UOs0v">
      <property role="2uHTBK" value="default_RTransform" />
      <ref role="3UNGvu" to="c3oy:7eZWuAL6RNp" resolve="External" />
      <node concept="tYCnQ" id="6fgLCPsCcYC" role="_1QTJ">
        <ref role="uz4UX" to="c3oy:7eZWuAL6RNp" resolve="External" />
        <node concept="Cmt7Y" id="6fgLCPsCd9W" role="uz6Si">
          <node concept="Cnhdc" id="6fgLCPsCd9Y" role="Cncma">
            <node concept="3clFbS" id="6fgLCPsCda0" role="2VODD2">
              <node concept="3clFbF" id="6fgLCPsD9ai" role="3cqZAp">
                <node concept="2OqwBi" id="6fgLCPsD9ui" role="3clFbG">
                  <node concept="2OqwBi" id="6fgLCPsD9cp" role="2Oq$k0">
                    <node concept="Cj7Ep" id="6fgLCPsD9ah" role="2Oq$k0" />
                    <node concept="3TrEf2" id="6fgLCPsD9kw" role="2OqNvi">
                      <ref role="3Tt5mk" to="c3oy:6fgLCPsD6B1" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="6fgLCPsD9Ew" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="6fgLCPsD9Ih" role="3cqZAp">
                <node concept="2OqwBi" id="6fgLCPsD9KE" role="3clFbG">
                  <node concept="Cj7Ep" id="6fgLCPsD9If" role="2Oq$k0" />
                  <node concept="3TrEf2" id="6fgLCPsDa0E" role="2OqNvi">
                    <ref role="3Tt5mk" to="c3oy:6fgLCPsD6B1" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="6fgLCPsCdaH" role="Cn2iK">
            <property role="2h1i$Z" value="&quot;" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="6b3yt4I31Pw" role="3UOs0v">
      <property role="7I3sp" value="left" />
      <ref role="3UNGvu" to="c3oy:7eZWuAL6RNp" resolve="External" />
      <node concept="tYCnQ" id="6b3yt4I37se" role="_1QTJ">
        <ref role="uz4UX" to="c3oy:7eZWuAL6RNp" resolve="External" />
        <node concept="Cmt7Y" id="6b3yt4I37sj" role="uz6Si">
          <node concept="Cnhdc" id="6b3yt4I37sl" role="Cncma">
            <node concept="3clFbS" id="6b3yt4I37sn" role="2VODD2">
              <node concept="3clFbF" id="6b3yt4I3bYI" role="3cqZAp">
                <node concept="2OqwBi" id="6b3yt4I3vkU" role="3clFbG">
                  <node concept="2OqwBi" id="6b3yt4I3ckP" role="2Oq$k0">
                    <node concept="Cj7Ep" id="6b3yt4I3bYH" role="2Oq$k0" />
                    <node concept="3TrEf2" id="6b3yt4I3vaB" role="2OqNvi">
                      <ref role="3Tt5mk" to="c3oy:6fgLCPsD6B1" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="6b3yt4I3vS4" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="6b3yt4I3vVB" role="3cqZAp">
                <node concept="2OqwBi" id="6b3yt4I3vXP" role="3clFbG">
                  <node concept="Cj7Ep" id="6b3yt4I3vV_" role="2Oq$k0" />
                  <node concept="3TrEf2" id="6b3yt4I3wdK" role="2OqNvi">
                    <ref role="3Tt5mk" to="c3oy:6fgLCPsD6B1" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="6b3yt4I37t1" role="Cn2iK">
            <property role="2h1i$Z" value="&quot;" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="6b3yt4I3wg9" role="3UOs0v">
      <ref role="3UNGvu" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
      <node concept="tYCnQ" id="6b3yt4I3Tqw" role="_1QTJ">
        <ref role="uz4UX" to="c3oy:7eZWuAL6RNp" resolve="External" />
        <node concept="Cmt7Y" id="6b3yt4I3TAT" role="uz6Si">
          <node concept="2h1dTh" id="6b3yt4I3TB$" role="Cn2iK">
            <property role="2h1i$Z" value="&quot;" />
          </node>
          <node concept="Cnhdc" id="6b3yt4I3TCa" role="Cncma">
            <node concept="3clFbS" id="6b3yt4I3TCb" role="2VODD2">
              <node concept="3cpWs8" id="6b3yt4I3Vy0" role="3cqZAp">
                <node concept="3cpWsn" id="6b3yt4I3Vy6" role="3cpWs9">
                  <property role="TrG5h" value="external" />
                  <node concept="3Tqbb2" id="6b3yt4I3Wrd" role="1tU5fm">
                    <ref role="ehGHo" to="c3oy:7eZWuAL6RNp" resolve="External" />
                  </node>
                  <node concept="10QFUN" id="6b3yt4I3Yfs" role="33vP2m">
                    <node concept="2OqwBi" id="6b3yt4I3XDN" role="10QFUP">
                      <node concept="Cj7Ep" id="6b3yt4I3XAD" role="2Oq$k0" />
                      <node concept="1mfA1w" id="6b3yt4I3XNx" role="2OqNvi" />
                    </node>
                    <node concept="3Tqbb2" id="6b3yt4I3Yft" role="10QFUM">
                      <ref role="ehGHo" to="c3oy:7eZWuAL6RNp" resolve="External" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="6b3yt4I3TIo" role="3cqZAp">
                <node concept="2OqwBi" id="6b3yt4I3UIW" role="3clFbG">
                  <node concept="2OqwBi" id="6b3yt4I3UoU" role="2Oq$k0">
                    <node concept="37vLTw" id="6b3yt4I3Yjv" role="2Oq$k0">
                      <ref role="3cqZAo" node="6b3yt4I3Vy6" resolve="external" />
                    </node>
                    <node concept="3TrEf2" id="6b3yt4I3UyY" role="2OqNvi">
                      <ref role="3Tt5mk" to="c3oy:6fgLCPsD6B1" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="6b3yt4I3UVP" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="6b3yt4I3Yqu" role="3cqZAp">
                <node concept="37vLTw" id="6b3yt4I3Yqs" role="3clFbG">
                  <ref role="3cqZAo" node="6b3yt4I3Vy6" resolve="external" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3kRJcU" id="6b3yt4I3whd" role="3kShCk">
        <node concept="3clFbS" id="6b3yt4I3whe" role="2VODD2">
          <node concept="3clFbF" id="6b3yt4I3PDi" role="3cqZAp">
            <node concept="2OqwBi" id="6b3yt4I3QlW" role="3clFbG">
              <node concept="2OqwBi" id="6b3yt4I3PJJ" role="2Oq$k0">
                <node concept="Cj7Ep" id="6b3yt4I3PDh" role="2Oq$k0" />
                <node concept="1mfA1w" id="6b3yt4I3PTB" role="2OqNvi" />
              </node>
              <node concept="1mIQ4w" id="6b3yt4I3QuA" role="2OqNvi">
                <node concept="chp4Y" id="6b3yt4I3Q$r" role="cj9EA">
                  <ref role="cht4Q" to="c3oy:7eZWuAL6RNp" resolve="External" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="dGdbRZiCT2">
    <property role="3GE5qa" value="" />
    <property role="TrG5h" value="addAliasConcept" />
    <node concept="3UNGvq" id="dGdbRZiCT3" role="3UOs0v">
      <ref role="3UNGvu" to="c3oy:7eZWuAL6NSF" resolve="Import" />
      <node concept="tYCnQ" id="dGdbRZiCUP" role="_1QTJ">
        <ref role="uz4UX" to="c3oy:7eZWuAL6NSF" resolve="Import" />
        <node concept="Cmt7Y" id="dGdbRZiCUS" role="uz6Si">
          <node concept="Cnhdc" id="dGdbRZiCUT" role="Cncma">
            <node concept="3clFbS" id="dGdbRZiCUU" role="2VODD2">
              <node concept="3clFbF" id="dGdbRZiCVA" role="3cqZAp">
                <node concept="2OqwBi" id="dGdbRZiDf4" role="3clFbG">
                  <node concept="2OqwBi" id="dGdbRZiCXk" role="2Oq$k0">
                    <node concept="Cj7Ep" id="dGdbRZiCV_" role="2Oq$k0" />
                    <node concept="3TrEf2" id="dGdbRZiD5m" role="2OqNvi">
                      <ref role="3Tt5mk" to="c3oy:dGdbRZiBfa" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="dGdbRZiDr6" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="dGdbRZiDux" role="3cqZAp">
                <node concept="2OqwBi" id="dGdbRZiJql" role="3clFbG">
                  <node concept="Cj7Ep" id="dGdbRZiJn$" role="2Oq$k0" />
                  <node concept="3TrEf2" id="dGdbRZiJEd" role="2OqNvi">
                    <ref role="3Tt5mk" to="c3oy:dGdbRZiBfa" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="dGdbRZiCVz" role="Cn2iK">
            <property role="2h1i$Z" value="as" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

