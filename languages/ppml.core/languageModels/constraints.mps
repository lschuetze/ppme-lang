<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7786317f-a0fc-4b70-958d-97731273a9d3(de.ppme.modules.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="c3oy" ref="r:8a0fa9ef-3e8e-4313-a85b-46b3640418fd(de.ppme.modules.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1092119917967" name="jetbrains.mps.baseLanguage.structure.MulExpression" flags="nn" index="17qRlL" />
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="1147467115080" name="jetbrains.mps.lang.constraints.structure.NodePropertyConstraint" flags="ng" index="EnEH3">
        <reference id="1147467295099" name="applicableProperty" index="EomxK" />
        <child id="1212097481299" name="propertyValidator" index="QCWH9" />
      </concept>
      <concept id="1147468365020" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_node" flags="nn" index="EsrRn" />
      <concept id="1212096972063" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_PropertyValidator" flags="in" index="QB0g5" />
      <concept id="8401916545537438642" name="jetbrains.mps.lang.constraints.structure.InheritedNodeScopeFactory" flags="ng" index="1dDu$B">
        <reference id="8401916545537438643" name="kind" index="1dDu$A" />
      </concept>
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213098023997" name="property" index="1MhHOB" />
        <child id="1213100494875" name="referent" index="1Mr941" />
      </concept>
      <concept id="1148687176410" name="jetbrains.mps.lang.constraints.structure.NodeReferentConstraint" flags="ng" index="1N5Pfh">
        <reference id="1148687202698" name="applicableLink" index="1N5Vy1" />
        <child id="1148687345559" name="searchScopeFactory" index="1N6uqs" />
      </concept>
      <concept id="1153138554286" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_propertyValue" flags="nn" index="1Wqviy" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
    </language>
  </registry>
  <node concept="1M2fIO" id="5rrFAeHUquK">
    <property role="3GE5qa" value="phase" />
    <ref role="1M2myG" to="c3oy:3yJ4drhX9yy" resolve="PhaseInputDeclaration" />
    <node concept="1N5Pfh" id="5rrFAeHUquL" role="1Mr941">
      <ref role="1N5Vy1" to="c3oy:5rrFAeHUiE8" />
      <node concept="1dDu$B" id="5rrFAeHUrfl" role="1N6uqs">
        <ref role="1dDu$A" to="c3oy:5o9jvTMaoHN" resolve="PhaseOutputDeclaration" />
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="2bnyqnQb6K3">
    <property role="3GE5qa" value="ppml" />
    <ref role="1M2myG" to="c3oy:2bnyqnQb6p4" resolve="DefineOpMacro" />
    <node concept="EnEH3" id="2bnyqnQb6K7" role="1MhHOB">
      <ref role="EomxK" to="c3oy:2bnyqnQb6IJ" resolve="ndim" />
      <node concept="QB0g5" id="2bnyqnQb6K9" role="QCWH9">
        <node concept="3clFbS" id="2bnyqnQb6Ka" role="2VODD2">
          <node concept="3cpWs6" id="2bnyqnQb8y9" role="3cqZAp">
            <node concept="1Wc70l" id="2bnyqnQbe0K" role="3cqZAk">
              <node concept="3clFbC" id="2bnyqnQbkR4" role="3uHU7w">
                <node concept="17qRlL" id="2bnyqnQblhp" role="3uHU7w">
                  <node concept="1Wqviy" id="2bnyqnQblog" role="3uHU7w" />
                  <node concept="1Wqviy" id="2bnyqnQbkV_" role="3uHU7B" />
                </node>
                <node concept="2OqwBi" id="2bnyqnQbfdr" role="3uHU7B">
                  <node concept="2OqwBi" id="2bnyqnQbecW" role="2Oq$k0">
                    <node concept="EsrRn" id="2bnyqnQbe7S" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="2bnyqnQbesi" role="2OqNvi">
                      <ref role="3TtcxE" to="c3oy:2bnyqnQb6FY" />
                    </node>
                  </node>
                  <node concept="34oBXx" id="2bnyqnQbh$a" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbC" id="2bnyqnQbd$N" role="3uHU7B">
                <node concept="2OqwBi" id="2bnyqnQb9G_" role="3uHU7B">
                  <node concept="2OqwBi" id="2bnyqnQb8I3" role="2Oq$k0">
                    <node concept="EsrRn" id="2bnyqnQb8DC" role="2Oq$k0" />
                    <node concept="3Tsc0h" id="2bnyqnQb8W5" role="2OqNvi">
                      <ref role="3TtcxE" to="c3oy:2bnyqnQb6EE" />
                    </node>
                  </node>
                  <node concept="34oBXx" id="2bnyqnQbc2x" role="2OqNvi" />
                </node>
                <node concept="1Wqviy" id="2bnyqnQbdFB" role="3uHU7w" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

