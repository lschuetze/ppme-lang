<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:6b58ab5c-75cb-4c00-a90d-4ee272fe38dd(de.ppme.modules.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="2gyk" ref="r:be57a480-399b-470c-860e-bdfcf98e0b2c(de.ppme.core.structure)" />
    <import index="c3oy" ref="r:8a0fa9ef-3e8e-4313-a85b-46b3640418fd(de.ppme.modules.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157993911077" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitProperty" flags="ng" index="2pJxcG">
        <reference id="5455284157993911078" name="property" index="2pJxcJ" />
      </concept>
      <concept id="5455284157993911097" name="jetbrains.mps.lang.quotation.structure.NodeBuilderInitPart" flags="ng" index="2pJxcK">
        <child id="5455284157993911094" name="expression" index="2pJxcZ" />
      </concept>
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
        <child id="5455284157993911099" name="values" index="2pJxcM" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1227096774658" name="jetbrains.mps.lang.typesystem.structure.MessageStatement" flags="ng" index="2OEH$v">
        <child id="1227096802790" name="nodeToReport" index="2OEOjV" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1YbPZF" id="52q84rKcrF1">
    <property role="TrG5h" value="typeof_DiscretizeOpMacro" />
    <property role="3GE5qa" value="ppml" />
    <node concept="3clFbS" id="52q84rKcrF2" role="18ibNy">
      <node concept="3SKdUt" id="52q84rKcrF8" role="3cqZAp">
        <node concept="3SKdUq" id="52q84rKcrFa" role="3SKWNk">
          <property role="3SKdUp" value="TODO: check typeof `operator` (DifferentialOperator)" />
        </node>
      </node>
      <node concept="3clFbH" id="52q84rKcsiD" role="3cqZAp" />
      <node concept="3clFbJ" id="52q84rKcrQ3" role="3cqZAp">
        <node concept="3clFbS" id="52q84rKcrQ5" role="3clFbx">
          <node concept="2MkqsV" id="52q84rKcsVd" role="3cqZAp">
            <node concept="2OqwBi" id="52q84rKctcX" role="2OEOjV">
              <node concept="1YBJjd" id="52q84rKct8C" role="2Oq$k0">
                <ref role="1YBMHb" node="52q84rKcrF4" resolve="discretizeOpMacro" />
              </node>
              <node concept="3TrEf2" id="52q84rKctvs" role="2OqNvi">
                <ref role="3Tt5mk" to="c3oy:52q84rKcrDv" />
              </node>
            </node>
            <node concept="3cpWs3" id="52q84rKct4B" role="2MkJ7o">
              <node concept="Xl_RD" id="52q84rKcsVO" role="3uHU7B">
                <property role="Xl_RC" value="Reference `discretization` must be of particle list type, but is " />
              </node>
              <node concept="1Z2H0r" id="52q84rKct59" role="3uHU7w">
                <node concept="2OqwBi" id="52q84rKct5a" role="1Z2MuG">
                  <node concept="1YBJjd" id="52q84rKct5b" role="2Oq$k0">
                    <ref role="1YBMHb" node="52q84rKcrF4" resolve="discretizeOpMacro" />
                  </node>
                  <node concept="3TrEf2" id="52q84rKct5c" role="2OqNvi">
                    <ref role="3Tt5mk" to="c3oy:52q84rKcrDv" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3fqX7Q" id="52q84rKcsTc" role="3clFbw">
          <node concept="2OqwBi" id="52q84rKcsTe" role="3fr31v">
            <node concept="1Z2H0r" id="52q84rKcsTf" role="2Oq$k0">
              <node concept="2OqwBi" id="52q84rKcsTg" role="1Z2MuG">
                <node concept="1YBJjd" id="52q84rKcsTh" role="2Oq$k0">
                  <ref role="1YBMHb" node="52q84rKcrF4" resolve="discretizeOpMacro" />
                </node>
                <node concept="3TrEf2" id="52q84rKcsTi" role="2OqNvi">
                  <ref role="3Tt5mk" to="c3oy:52q84rKcrDv" />
                </node>
              </node>
            </node>
            <node concept="1mIQ4w" id="52q84rKcsTj" role="2OqNvi">
              <node concept="chp4Y" id="52q84rKcsTk" role="cj9EA">
                <ref role="cht4Q" to="2gyk:6Wx7SFgabxi" resolve="ParticleListType" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="52q84rKfP7E" role="3cqZAp" />
      <node concept="1Z5TYs" id="52q84rKfPaq" role="3cqZAp">
        <node concept="mw_s8" id="52q84rKfPb6" role="1ZfhKB">
          <node concept="2pJPEk" id="52q84rKfPb2" role="mwGJk">
            <node concept="2pJPED" id="52q84rKfPbh" role="2pJPEn">
              <ref role="2pJxaS" to="2gyk:52q84rKcUdJ" resolve="DifferentialOperatorType" />
              <node concept="2pJxcG" id="52q84rKfPbv" role="2pJxcM">
                <ref role="2pJxcJ" to="2gyk:52q84rKcUg6" resolve="discretized" />
                <node concept="3clFbT" id="52q84rKfPbL" role="2pJxcZ">
                  <property role="3clFbU" value="true" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="52q84rKfPat" role="1ZfhK$">
          <node concept="1Z2H0r" id="52q84rKfP8t" role="mwGJk">
            <node concept="1YBJjd" id="52q84rKfP9j" role="1Z2MuG">
              <ref role="1YBMHb" node="52q84rKcrF4" resolve="discretizeOpMacro" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="52q84rKcrF4" role="1YuTPh">
      <property role="TrG5h" value="discretizeOpMacro" />
      <ref role="1YaFvo" to="c3oy:52q84rKcqDg" resolve="DiscretizeOpMacro" />
    </node>
  </node>
  <node concept="1YbPZF" id="52q84rKcUmj">
    <property role="TrG5h" value="typeof_DefineOpMacro" />
    <property role="3GE5qa" value="ppml" />
    <node concept="3clFbS" id="52q84rKcUmk" role="18ibNy">
      <node concept="1Z5TYs" id="52q84rKcUnX" role="3cqZAp">
        <node concept="mw_s8" id="52q84rKcUoh" role="1ZfhKB">
          <node concept="2pJPEk" id="52q84rKcUod" role="mwGJk">
            <node concept="2pJPED" id="52q84rKcUos" role="2pJPEn">
              <ref role="2pJxaS" to="2gyk:52q84rKcUdJ" resolve="DifferentialOperatorType" />
              <node concept="2pJxcG" id="52q84rKcUpx" role="2pJxcM">
                <ref role="2pJxcJ" to="2gyk:52q84rKcUg6" resolve="discretized" />
                <node concept="3clFbT" id="52q84rKcUpN" role="2pJxcZ">
                  <property role="3clFbU" value="false" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="52q84rKcUo0" role="1ZfhK$">
          <node concept="1Z2H0r" id="52q84rKcUmq" role="mwGJk">
            <node concept="1YBJjd" id="52q84rKcUmQ" role="1Z2MuG">
              <ref role="1YBMHb" node="52q84rKcUmm" resolve="defineOpMacro" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="52q84rKcUmm" role="1YuTPh">
      <property role="TrG5h" value="defineOpMacro" />
      <ref role="1YaFvo" to="c3oy:2bnyqnQb6p4" resolve="DefineOpMacro" />
    </node>
  </node>
</model>

