<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:84ce93e4-8a21-447c-8911-c0a4415308db(de.ppme.base.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpch" ref="r:00000000-0000-4000-0000-011c8959028d(jetbrains.mps.lang.structure.editor)" />
    <import index="9a8" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.nodeEditor(MPS.Editor/jetbrains.mps.nodeEditor@java_stub)" />
    <import index="1t7x" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.awt(JDK/java.awt@java_stub)" />
    <import index="jsgz" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.nodeEditor.cells(MPS.Editor/jetbrains.mps.nodeEditor.cells@java_stub)" />
    <import index="ar19" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.awt.geom(JDK/java.awt.geom@java_stub)" />
    <import index="nu8v" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.openapi.editor.cells(MPS.Editor/jetbrains.mps.openapi.editor.cells@java_stub)" />
    <import index="srng" ref="1ed103c3-3aa6-49b7-9c21-6765ee11f224/f:java_stub#1ed103c3-3aa6-49b7-9c21-6765ee11f224#jetbrains.mps.openapi.editor(MPS.Editor/jetbrains.mps.openapi.editor@java_stub)" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1186402211651" name="jetbrains.mps.lang.editor.structure.StyleSheet" flags="ng" index="V5hpn">
        <child id="1186402402630" name="styleClass" index="V601i" />
      </concept>
      <concept id="1186403694788" name="jetbrains.mps.lang.editor.structure.ColorStyleClassItem" flags="ln" index="VaVBg">
        <property id="1186403713874" name="color" index="Vb096" />
        <child id="1186403803051" name="query" index="VblUZ" />
      </concept>
      <concept id="1186403751766" name="jetbrains.mps.lang.editor.structure.FontStyleStyleClassItem" flags="ln" index="Vb9p2">
        <property id="1186403771423" name="style" index="Vbekb" />
      </concept>
      <concept id="1186404549998" name="jetbrains.mps.lang.editor.structure.ForegroundColorStyleClassItem" flags="ln" index="VechU" />
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1240253180846" name="jetbrains.mps.lang.editor.structure.IndentLayoutNoWrapClassItem" flags="ln" index="34QqEe" />
      <concept id="3383245079137382180" name="jetbrains.mps.lang.editor.structure.StyleClass" flags="ig" index="14StLt" />
      <concept id="1225456267680" name="jetbrains.mps.lang.editor.structure.RGBColor" flags="ng" index="1iSF2X">
        <property id="1225456424731" name="value" index="1iTho6" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="9122903797312246523" name="jetbrains.mps.lang.editor.structure.StyleReference" flags="ng" index="1wgc9g">
        <reference id="9122903797312247166" name="style" index="1wgcnl" />
      </concept>
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1950447826681509042" name="jetbrains.mps.lang.editor.structure.ApplyStyleClass" flags="lg" index="3Xmtl4">
        <child id="1950447826683828796" name="target" index="3XvnJa" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1224848483129" name="jetbrains.mps.baseLanguage.structure.IBLDeprecatable" flags="ng" index="IEa8$">
        <property id="1224848525476" name="isDeprecated" index="IEkAT" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475354124" name="jetbrains.mps.baseLanguage.structure.ThisExpression" flags="nn" index="Xjq3P">
        <reference id="1182955020723" name="classConcept" index="1HBi2w" />
      </concept>
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1182160077978" name="jetbrains.mps.baseLanguage.structure.AnonymousClassCreator" flags="nn" index="YeOm9">
        <child id="1182160096073" name="cls" index="YeSDq" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534513062" name="jetbrains.mps.baseLanguage.structure.DoubleType" flags="in" index="10P55v" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1111509017652" name="jetbrains.mps.baseLanguage.structure.FloatingPointConstant" flags="nn" index="3b6qkQ">
        <property id="1113006610751" name="value" index="$nhwW" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1170345865475" name="jetbrains.mps.baseLanguage.structure.AnonymousClass" flags="ig" index="1Y3b0j">
        <reference id="1170346070688" name="classifier" index="1Y3XeK" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="V5hpn" id="5l83jlMfPVn">
    <property role="TrG5h" value="ppmlBaseStyles" />
    <node concept="14StLt" id="5l83jlMfRXj" role="V601i">
      <property role="TrG5h" value="Boolean" />
      <node concept="3Xmtl4" id="5l83jlMfRXq" role="3F10Kt">
        <node concept="1wgc9g" id="5l83jlMfRXy" role="3XvnJa">
          <ref role="1wgcnl" to="tpch:24YP6ZDyde4" resolve="Keyword" />
        </node>
      </node>
    </node>
    <node concept="14StLt" id="6fgLCPsD6$p" role="V601i">
      <property role="TrG5h" value="String" />
      <node concept="VechU" id="6fgLCPsD6$W" role="3F10Kt">
        <node concept="1iSF2X" id="6fgLCPsD6_0" role="VblUZ">
          <property role="1iTho6" value="008000" />
        </node>
      </node>
      <node concept="Vb9p2" id="6fgLCPsD6_a" role="3F10Kt" />
    </node>
    <node concept="14StLt" id="5l83jlMgWkl" role="V601i">
      <property role="TrG5h" value="Operator" />
      <node concept="Vb9p2" id="5l83jlMgWky" role="3F10Kt" />
      <node concept="VechU" id="5l83jlMgWkF" role="3F10Kt">
        <property role="Vb096" value="black" />
      </node>
    </node>
    <node concept="14StLt" id="5l83jlMgW$8" role="V601i">
      <property role="TrG5h" value="Parenthesis" />
      <node concept="34QqEe" id="5l83jlMgW$O" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
      <node concept="3mYdg7" id="5l83jlMh4vD" role="3F10Kt">
        <property role="1413C4" value="parenthesis" />
      </node>
      <node concept="Vb9p2" id="5l83jlMgW_1" role="3F10Kt" />
    </node>
    <node concept="14StLt" id="5l83jlMhrlw" role="V601i">
      <property role="TrG5h" value="Semicolon" />
      <node concept="VechU" id="5l83jlMhrlV" role="3F10Kt">
        <property role="Vb096" value="darkGray" />
      </node>
      <node concept="Vb9p2" id="5l83jlMhrm4" role="3F10Kt">
        <property role="Vbekb" value="PLAIN" />
      </node>
      <node concept="11L4FC" id="5l83jlMhrmq" role="3F10Kt">
        <property role="VOm3f" value="true" />
      </node>
    </node>
  </node>
  <node concept="312cEu" id="7vxxPMIj1UZ">
    <property role="TrG5h" value="BracketedCellProvider" />
    <node concept="2tJIrI" id="7vxxPMIjgeg" role="jymVt" />
    <node concept="312cEg" id="7vxxPMIjghL" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myNode" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tmbuc" id="7vxxPMIjggr" role="1B3o_S" />
      <node concept="3Tqbb2" id="7vxxPMIjgho" role="1tU5fm" />
    </node>
    <node concept="312cEg" id="7vxxPMIjiI4" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myColor" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tmbuc" id="7vxxPMIjgmD" role="1B3o_S" />
      <node concept="3uibUv" id="7vxxPMIjiH_" role="1tU5fm">
        <ref role="3uigEE" to="1t7x:~Color" resolve="Color" />
      </node>
    </node>
    <node concept="312cEg" id="7vxxPMIowRm" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="isRight" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tmbuc" id="7vxxPMIowuo" role="1B3o_S" />
      <node concept="10P_77" id="7vxxPMIowRj" role="1tU5fm" />
    </node>
    <node concept="2tJIrI" id="7vxxPMIjiJb" role="jymVt" />
    <node concept="3clFbW" id="7vxxPMIjiKQ" role="jymVt">
      <node concept="3cqZAl" id="7vxxPMIjiKR" role="3clF45" />
      <node concept="3Tm1VV" id="7vxxPMIjiKS" role="1B3o_S" />
      <node concept="3clFbS" id="7vxxPMIjiKU" role="3clF47">
        <node concept="XkiVB" id="7vxxPMIjjO8" role="3cqZAp">
          <ref role="37wK5l" to="9a8:~AbstractCellProvider.&lt;init&gt;()" resolve="AbstractCellProvider" />
        </node>
        <node concept="3clFbF" id="7vxxPMIjiL3" role="3cqZAp">
          <node concept="37vLTI" id="7vxxPMIjl8L" role="3clFbG">
            <node concept="37vLTw" id="7vxxPMIjlan" role="37vLTx">
              <ref role="3cqZAo" node="7vxxPMIjiL2" resolve="node" />
            </node>
            <node concept="2OqwBi" id="7vxxPMIjkB1" role="37vLTJ">
              <node concept="Xjq3P" id="7vxxPMIjjRa" role="2Oq$k0" />
              <node concept="2OwXpG" id="7vxxPMIjkRe" role="2OqNvi">
                <ref role="2Oxat5" node="7vxxPMIjghL" resolve="myNode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7vxxPMIkHCL" role="3cqZAp">
          <node concept="37vLTI" id="7vxxPMIkI4R" role="3clFbG">
            <node concept="10M0yZ" id="7vxxPMIkI9J" role="37vLTx">
              <ref role="3cqZAo" to="1t7x:~Color.GRAY" resolve="GRAY" />
              <ref role="1PxDUh" to="1t7x:~Color" resolve="Color" />
            </node>
            <node concept="2OqwBi" id="7vxxPMIkHGh" role="37vLTJ">
              <node concept="Xjq3P" id="7vxxPMIkHCJ" role="2Oq$k0" />
              <node concept="2OwXpG" id="7vxxPMIkHWF" role="2OqNvi">
                <ref role="2Oxat5" node="7vxxPMIjiI4" resolve="myColor" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="7vxxPMIoxnW" role="3cqZAp">
          <node concept="37vLTI" id="7vxxPMIoxOl" role="3clFbG">
            <node concept="37vLTw" id="7vxxPMIoxRV" role="37vLTx">
              <ref role="3cqZAo" node="7vxxPMIoxjf" resolve="isRight" />
            </node>
            <node concept="2OqwBi" id="7vxxPMIoxrO" role="37vLTJ">
              <node concept="Xjq3P" id="7vxxPMIoxnU" role="2Oq$k0" />
              <node concept="2OwXpG" id="7vxxPMIoxGq" role="2OqNvi">
                <ref role="2Oxat5" node="7vxxPMIowRm" resolve="isRight" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="7vxxPMIjiL2" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="7vxxPMIjiL1" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="7vxxPMIoxjf" role="3clF46">
        <property role="TrG5h" value="isRight" />
        <node concept="10P_77" id="7vxxPMIoxlv" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="7vxxPMIjiJI" role="jymVt" />
    <node concept="3Tm1VV" id="7vxxPMIj1V0" role="1B3o_S" />
    <node concept="3uibUv" id="7vxxPMIjfIY" role="1zkMxy">
      <ref role="3uigEE" to="9a8:~AbstractCellProvider" resolve="AbstractCellProvider" />
    </node>
    <node concept="3clFb_" id="7vxxPMIl1bf" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="createEditorCell" />
      <property role="DiZV1" value="false" />
      <property role="IEkAT" value="false" />
      <node concept="3Tm1VV" id="7vxxPMIl1bg" role="1B3o_S" />
      <node concept="3uibUv" id="7vxxPMIl1bi" role="3clF45">
        <ref role="3uigEE" to="nu8v:~EditorCell" resolve="EditorCell" />
      </node>
      <node concept="37vLTG" id="7vxxPMIl1bj" role="3clF46">
        <property role="TrG5h" value="context" />
        <node concept="3uibUv" id="7vxxPMIl1bk" role="1tU5fm">
          <ref role="3uigEE" to="srng:~EditorContext" resolve="EditorContext" />
        </node>
      </node>
      <node concept="3clFbS" id="7vxxPMIl1bl" role="3clF47">
        <node concept="3cpWs8" id="7vxxPMIkLSo" role="3cqZAp">
          <node concept="3cpWsn" id="7vxxPMIjzC0" role="3cpWs9">
            <property role="TrG5h" value="cell" />
            <node concept="3uibUv" id="7vxxPMIjzC1" role="1tU5fm">
              <ref role="3uigEE" to="jsgz:~EditorCell_Basic" resolve="EditorCell_Basic" />
            </node>
            <node concept="2ShNRf" id="7vxxPMIkiMV" role="33vP2m">
              <node concept="YeOm9" id="7vxxPMIkGS6" role="2ShVmc">
                <node concept="1Y3b0j" id="7vxxPMIkGS9" role="YeSDq">
                  <property role="2bfB8j" value="true" />
                  <ref role="37wK5l" to="jsgz:~EditorCell_Basic.&lt;init&gt;(jetbrains.mps.openapi.editor.EditorContext,org.jetbrains.mps.openapi.model.SNode)" resolve="EditorCell_Basic" />
                  <ref role="1Y3XeK" to="jsgz:~EditorCell_Basic" resolve="EditorCell_Basic" />
                  <node concept="3Tm1VV" id="7vxxPMIkGSa" role="1B3o_S" />
                  <node concept="3clFb_" id="7vxxPMIkGSb" role="jymVt">
                    <property role="1EzhhJ" value="false" />
                    <property role="TrG5h" value="paintContent" />
                    <property role="DiZV1" value="false" />
                    <property role="IEkAT" value="false" />
                    <node concept="3Tmbuc" id="7vxxPMIkGSc" role="1B3o_S" />
                    <node concept="3cqZAl" id="7vxxPMIkGSe" role="3clF45" />
                    <node concept="37vLTG" id="7vxxPMIkGSf" role="3clF46">
                      <property role="TrG5h" value="g" />
                      <node concept="3uibUv" id="7vxxPMIkGSg" role="1tU5fm">
                        <ref role="3uigEE" to="1t7x:~Graphics" resolve="Graphics" />
                      </node>
                    </node>
                    <node concept="37vLTG" id="7vxxPMIkGSh" role="3clF46">
                      <property role="TrG5h" value="parentSettings" />
                      <node concept="3uibUv" id="7vxxPMIkGSi" role="1tU5fm">
                        <ref role="3uigEE" to="jsgz:~ParentSettings" resolve="ParentSettings" />
                      </node>
                    </node>
                    <node concept="3clFbS" id="7vxxPMIkGSj" role="3clF47">
                      <node concept="3clFbF" id="7vxxPMIlyWf" role="3cqZAp">
                        <node concept="2OqwBi" id="7vxxPMIlz2C" role="3clFbG">
                          <node concept="37vLTw" id="7vxxPMIlyWd" role="2Oq$k0">
                            <ref role="3cqZAo" node="7vxxPMIkGSf" resolve="g" />
                          </node>
                          <node concept="liA8E" id="7vxxPMIlzop" role="2OqNvi">
                            <ref role="37wK5l" to="1t7x:~Graphics.setColor(java.awt.Color):void" resolve="setColor" />
                            <node concept="37vLTw" id="7vxxPMIlzrb" role="37wK5m">
                              <ref role="3cqZAo" node="7vxxPMIjiI4" resolve="myColor" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbH" id="7vxxPMIlyL$" role="3cqZAp" />
                      <node concept="3cpWs8" id="1F0U9H74lad" role="3cqZAp">
                        <node concept="3cpWsn" id="1F0U9H74lae" role="3cpWs9">
                          <property role="TrG5h" value="parent" />
                          <node concept="3uibUv" id="1F0U9H74laf" role="1tU5fm">
                            <ref role="3uigEE" to="jsgz:~EditorCell_Collection" resolve="EditorCell_Collection" />
                          </node>
                          <node concept="2OqwBi" id="1F0U9H74lag" role="33vP2m">
                            <node concept="liA8E" id="1F0U9H74lah" role="2OqNvi">
                              <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getParent():jetbrains.mps.nodeEditor.cells.EditorCell_Collection" resolve="getParent" />
                            </node>
                            <node concept="Xjq3P" id="1F0U9H74lai" role="2Oq$k0" />
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbH" id="7vxxPMIpj1t" role="3cqZAp" />
                      <node concept="3SKdUt" id="7vxxPMIpu_e" role="3cqZAp">
                        <node concept="3SKdUq" id="7vxxPMIpuRf" role="3SKWNk">
                          <property role="3SKdUp" value="line width" />
                        </node>
                      </node>
                      <node concept="3cpWs8" id="7vxxPMIpj$O" role="3cqZAp">
                        <node concept="3cpWsn" id="7vxxPMIpj$M" role="3cpWs9">
                          <property role="3TUv4t" value="true" />
                          <property role="TrG5h" value="w" />
                          <node concept="10P55v" id="7vxxPMIpjBJ" role="1tU5fm" />
                          <node concept="3b6qkQ" id="7vxxPMIpojA" role="33vP2m">
                            <property role="$nhwW" value="1.8" />
                          </node>
                        </node>
                      </node>
                      <node concept="3SKdUt" id="7vxxPMIpvmh" role="3cqZAp">
                        <node concept="3SKdUq" id="7vxxPMIpvCm" role="3SKWNk">
                          <property role="3SKdUp" value="hook lenght" />
                        </node>
                      </node>
                      <node concept="3cpWs8" id="7vxxPMIpw7k" role="3cqZAp">
                        <node concept="3cpWsn" id="7vxxPMIpw7i" role="3cpWs9">
                          <property role="3TUv4t" value="true" />
                          <property role="TrG5h" value="d" />
                          <node concept="10P55v" id="7vxxPMIpwpb" role="1tU5fm" />
                          <node concept="3b6qkQ" id="7vxxPMIpQaW" role="33vP2m">
                            <property role="$nhwW" value="4.8" />
                          </node>
                        </node>
                      </node>
                      <node concept="3SKdUt" id="7vxxPMIpwtg" role="3cqZAp">
                        <node concept="3SKdUq" id="7vxxPMIpwJx" role="3SKWNk">
                          <property role="3SKdUp" value="left" />
                        </node>
                      </node>
                      <node concept="3cpWs8" id="7vxxPMIpjfs" role="3cqZAp">
                        <node concept="3cpWsn" id="7vxxPMIpjfq" role="3cpWs9">
                          <property role="3TUv4t" value="true" />
                          <property role="TrG5h" value="l" />
                          <node concept="10P55v" id="7vxxPMIpjih" role="1tU5fm" />
                          <node concept="3b6qkQ" id="7vxxPMIpjOD" role="33vP2m">
                            <property role="$nhwW" value="2.0" />
                          </node>
                        </node>
                      </node>
                      <node concept="3SKdUt" id="7vxxPMIpxtg" role="3cqZAp">
                        <node concept="3SKdUq" id="7vxxPMIpxJs" role="3SKWNk">
                          <property role="3SKdUp" value="right" />
                        </node>
                      </node>
                      <node concept="3cpWs8" id="7vxxPMIpk0_" role="3cqZAp">
                        <node concept="3cpWsn" id="7vxxPMIpk0z" role="3cpWs9">
                          <property role="3TUv4t" value="true" />
                          <property role="TrG5h" value="r" />
                          <node concept="10P55v" id="7vxxPMIpkaS" role="1tU5fm" />
                          <node concept="3cpWs3" id="7vxxPMIpkmU" role="33vP2m">
                            <node concept="37vLTw" id="7vxxPMIpkom" role="3uHU7w">
                              <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                            </node>
                            <node concept="37vLTw" id="7vxxPMIpkcl" role="3uHU7B">
                              <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3SKdUt" id="7vxxPMIpxZT" role="3cqZAp">
                        <node concept="3SKdUq" id="7vxxPMIpyig" role="3SKWNk">
                          <property role="3SKdUp" value="top" />
                        </node>
                      </node>
                      <node concept="3cpWs8" id="7vxxPMIpkEE" role="3cqZAp">
                        <node concept="3cpWsn" id="7vxxPMIpkEC" role="3cpWs9">
                          <property role="3TUv4t" value="true" />
                          <property role="TrG5h" value="t" />
                          <node concept="10P55v" id="7vxxPMIpkHX" role="1tU5fm" />
                          <node concept="3b6qkQ" id="7vxxPMIpkQH" role="33vP2m">
                            <property role="$nhwW" value="-3.0" />
                          </node>
                        </node>
                      </node>
                      <node concept="3SKdUt" id="7vxxPMIpyN4" role="3cqZAp">
                        <node concept="3SKdUq" id="7vxxPMIpz5m" role="3SKWNk">
                          <property role="3SKdUp" value="bottom" />
                        </node>
                      </node>
                      <node concept="3cpWs8" id="7vxxPMIpl4c" role="3cqZAp">
                        <node concept="3cpWsn" id="7vxxPMIpl4a" role="3cpWs9">
                          <property role="3TUv4t" value="true" />
                          <property role="TrG5h" value="b" />
                          <node concept="10P55v" id="7vxxPMIpl7B" role="1tU5fm" />
                          <node concept="3cpWs3" id="7vxxPMIprtS" role="33vP2m">
                            <node concept="2OqwBi" id="7vxxPMIpqg5" role="3uHU7B">
                              <node concept="37vLTw" id="7vxxPMIppZJ" role="2Oq$k0">
                                <ref role="3cqZAo" node="1F0U9H74lae" resolve="parent" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpraO" role="2OqNvi">
                                <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getHeight():int" resolve="getHeight" />
                              </node>
                            </node>
                            <node concept="3cmrfG" id="7vxxPMIq2eX" role="3uHU7w">
                              <property role="3cmrfH" value="3" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbH" id="3VQE5sADEnX" role="3cqZAp" />
                      <node concept="3cpWs8" id="3VQE5sAfwXS" role="3cqZAp">
                        <node concept="3cpWsn" id="3VQE5sAfwXR" role="3cpWs9">
                          <property role="3TUv4t" value="false" />
                          <property role="TrG5h" value="s" />
                          <node concept="3uibUv" id="3VQE5sAfwXT" role="1tU5fm">
                            <ref role="3uigEE" to="ar19:~GeneralPath" resolve="GeneralPath" />
                          </node>
                          <node concept="2ShNRf" id="3VQE5sAfx15" role="33vP2m">
                            <node concept="1pGfFk" id="3VQE5sAfx16" role="2ShVmc">
                              <ref role="37wK5l" to="ar19:~GeneralPath.&lt;init&gt;()" resolve="GeneralPath" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbH" id="7vxxPMIpiRw" role="3cqZAp" />
                      <node concept="3clFbH" id="7vxxPMIoAaD" role="3cqZAp" />
                      <node concept="3clFbJ" id="7vxxPMIpRgy" role="3cqZAp">
                        <node concept="3clFbS" id="7vxxPMIpRg$" role="3clFbx">
                          <node concept="3clFbF" id="3VQE5sAfwZR" role="3cqZAp">
                            <node concept="2OqwBi" id="3VQE5sAfx1b" role="3clFbG">
                              <node concept="37vLTw" id="3VQE5sAfx1a" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="3VQE5sAfx1c" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.moveTo(double,double):void" resolve="moveTo" />
                                <node concept="37vLTw" id="7vxxPMIp$gF" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                </node>
                                <node concept="37vLTw" id="7vxxPMIp$ju" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="3VQE5sAfx07" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIp_Q6" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIp_Di" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpAaZ" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="37vLTw" id="7vxxPMIpAcP" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                </node>
                                <node concept="37vLTw" id="7vxxPMIpAuD" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpBbd" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpBo0" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpBbb" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpBLy" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="37vLTw" id="7vxxPMIpBNE" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                </node>
                                <node concept="37vLTw" id="7vxxPMIpC5w" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpCoE" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpC_M" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpCoC" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpCZY" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="3cpWsd" id="7vxxPMIpD9Z" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpDb8" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpD26" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                </node>
                                <node concept="37vLTw" id="7vxxPMIpDg0" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpD_s" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpDMH" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpD_q" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpEdH" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="3cpWsd" id="7vxxPMIpEmX" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpEo6" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpEfz" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                </node>
                                <node concept="3cpWsd" id="7vxxPMIpEAf" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpEBo" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpEsy" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpEXY" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpFbO" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpEXW" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpFB$" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="37vLTw" id="7vxxPMIpFDA" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                </node>
                                <node concept="3cpWsd" id="7vxxPMIpGb_" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpGcI" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpG44" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpGzs" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpGLJ" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpGzq" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpHel" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="37vLTw" id="7vxxPMIpHgn" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                </node>
                                <node concept="3cpWs3" id="7vxxPMIpHEg" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpHFp" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpHyd" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpI6b" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpImm" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpI69" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpIPm" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="3cpWsd" id="7vxxPMIpIYM" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpIZV" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpIRo" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                </node>
                                <node concept="3cpWs3" id="7vxxPMIpJee" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpJfn" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpJ4x" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpJGt" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpJXd" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpJGr" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpKt6" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="3cpWsd" id="7vxxPMIpKAy" role="37wK5m">
                                  <node concept="37vLTw" id="7vxxPMIpKBF" role="3uHU7w">
                                    <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpKv8" role="3uHU7B">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                </node>
                                <node concept="37vLTw" id="7vxxPMIpKGc" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpL7i" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpLov" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpL7g" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpLTw" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="37vLTw" id="7vxxPMIpLVg" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                </node>
                                <node concept="37vLTw" id="7vxxPMIpM53" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="3clFbF" id="7vxxPMIpMvF" role="3cqZAp">
                            <node concept="2OqwBi" id="7vxxPMIpMLd" role="3clFbG">
                              <node concept="37vLTw" id="7vxxPMIpMvD" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="7vxxPMIpNiR" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                <node concept="37vLTw" id="7vxxPMIpNkC" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                </node>
                                <node concept="37vLTw" id="7vxxPMIpNAs" role="37wK5m">
                                  <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="7vxxPMIpREK" role="3clFbw">
                          <ref role="3cqZAo" node="7vxxPMIowRm" resolve="isRight" />
                        </node>
                        <node concept="9aQIb" id="7vxxPMIpVJH" role="9aQIa">
                          <node concept="3clFbS" id="7vxxPMIpVJI" role="9aQI4">
                            <node concept="3clFbF" id="7vxxPMIpW5M" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW5N" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW5O" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW5P" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.moveTo(double,double):void" resolve="moveTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWrf" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpWtZ" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW5S" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW5T" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW5U" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW5V" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWwi" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpW5X" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW5Y" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW5Z" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW60" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW61" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWyD" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpW63" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW64" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW65" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW66" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW67" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="3cpWs3" id="7vxxPMIpZkx" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpW$X" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW69" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                    </node>
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpW6b" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6c" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6d" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW6e" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW6f" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="3cpWs3" id="7vxxPMIpZuv" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpWBs" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6h" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                    </node>
                                  </node>
                                  <node concept="3cpWsd" id="7vxxPMIpW6j" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpW6k" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6l" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6m" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6n" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW6o" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW6p" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWE1" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                  </node>
                                  <node concept="3cpWsd" id="7vxxPMIpW6r" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpW6s" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6t" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpl4a" resolve="b" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6u" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6v" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW6w" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW6x" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWGR" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                  </node>
                                  <node concept="3cpWs3" id="7vxxPMIpW6z" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpW6$" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6_" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6A" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6B" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW6C" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW6D" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="3cpWs3" id="7vxxPMIpZCD" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpWM6" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6F" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                    </node>
                                  </node>
                                  <node concept="3cpWs3" id="7vxxPMIpW6H" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpW6I" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpj$M" resolve="w" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6J" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6K" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6L" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW6M" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW6N" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="3cpWs3" id="7vxxPMIpZMN" role="37wK5m">
                                    <node concept="37vLTw" id="7vxxPMIpWQZ" role="3uHU7B">
                                      <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                    </node>
                                    <node concept="37vLTw" id="7vxxPMIpW6P" role="3uHU7w">
                                      <ref role="3cqZAo" node="7vxxPMIpw7i" resolve="d" />
                                    </node>
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpW6R" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6S" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6T" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW6U" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW6V" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWTs" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpk0z" resolve="r" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpW6X" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                  </node>
                                </node>
                              </node>
                            </node>
                            <node concept="3clFbF" id="7vxxPMIpW6Y" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpW6Z" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpW70" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpW71" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.lineTo(double,double):void" resolve="lineTo" />
                                  <node concept="37vLTw" id="7vxxPMIpWVN" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpjfq" resolve="l" />
                                  </node>
                                  <node concept="37vLTw" id="7vxxPMIpW73" role="37wK5m">
                                    <ref role="3cqZAo" node="7vxxPMIpkEC" resolve="t" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbH" id="3VQE5sAfCto" role="3cqZAp" />
                      <node concept="3clFbJ" id="7vxxPMIp93i" role="3cqZAp">
                        <node concept="3clFbS" id="7vxxPMIp93k" role="3clFbx">
                          <node concept="3clFbF" id="3VQE5sAu1GX" role="3cqZAp">
                            <node concept="2OqwBi" id="3VQE5sAu2qp" role="3clFbG">
                              <node concept="37vLTw" id="3VQE5sAu1GV" role="2Oq$k0">
                                <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                              </node>
                              <node concept="liA8E" id="3VQE5sAu2Qc" role="2OqNvi">
                                <ref role="37wK5l" to="ar19:~Path2D$Float.transform(java.awt.geom.AffineTransform):void" resolve="transform" />
                                <node concept="2YIFZM" id="3VQE5sAu2US" role="37wK5m">
                                  <ref role="1Pybhc" to="ar19:~AffineTransform" resolve="AffineTransform" />
                                  <ref role="37wK5l" to="ar19:~AffineTransform.getTranslateInstance(double,double):java.awt.geom.AffineTransform" resolve="getTranslateInstance" />
                                  <node concept="1rXfSq" id="3VQE5sAK4k5" role="37wK5m">
                                    <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getX():int" resolve="getX" />
                                  </node>
                                  <node concept="2OqwBi" id="3VQE5sBdsIY" role="37wK5m">
                                    <node concept="37vLTw" id="3VQE5sBdssF" role="2Oq$k0">
                                      <ref role="3cqZAo" node="1F0U9H74lae" resolve="parent" />
                                    </node>
                                    <node concept="liA8E" id="3VQE5sBdt$B" role="2OqNvi">
                                      <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getY():int" resolve="getY" />
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                        <node concept="37vLTw" id="7vxxPMIp9mV" role="3clFbw">
                          <ref role="3cqZAo" node="7vxxPMIowRm" resolve="isRight" />
                        </node>
                        <node concept="9aQIb" id="7vxxPMIpa6$" role="9aQIa">
                          <node concept="3clFbS" id="7vxxPMIpa6_" role="9aQI4">
                            <node concept="3clFbF" id="7vxxPMIpahI" role="3cqZAp">
                              <node concept="2OqwBi" id="7vxxPMIpahJ" role="3clFbG">
                                <node concept="37vLTw" id="7vxxPMIpahK" role="2Oq$k0">
                                  <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                                </node>
                                <node concept="liA8E" id="7vxxPMIpahL" role="2OqNvi">
                                  <ref role="37wK5l" to="ar19:~Path2D$Float.transform(java.awt.geom.AffineTransform):void" resolve="transform" />
                                  <node concept="2YIFZM" id="7vxxPMIpahM" role="37wK5m">
                                    <ref role="1Pybhc" to="ar19:~AffineTransform" resolve="AffineTransform" />
                                    <ref role="37wK5l" to="ar19:~AffineTransform.getTranslateInstance(double,double):java.awt.geom.AffineTransform" resolve="getTranslateInstance" />
                                    <node concept="3cpWsd" id="7vxxPMIpnZy" role="37wK5m">
                                      <node concept="3cmrfG" id="7vxxPMIpo0F" role="3uHU7w">
                                        <property role="3cmrfH" value="2" />
                                      </node>
                                      <node concept="1rXfSq" id="7vxxPMIpahO" role="3uHU7B">
                                        <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getX():int" resolve="getX" />
                                      </node>
                                    </node>
                                    <node concept="2OqwBi" id="7vxxPMIpahQ" role="37wK5m">
                                      <node concept="37vLTw" id="7vxxPMIpahR" role="2Oq$k0">
                                        <ref role="3cqZAo" node="1F0U9H74lae" resolve="parent" />
                                      </node>
                                      <node concept="liA8E" id="7vxxPMIpahS" role="2OqNvi">
                                        <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getY():int" resolve="getY" />
                                      </node>
                                    </node>
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3clFbH" id="3VQE5sAu0YC" role="3cqZAp" />
                      <node concept="3clFbF" id="3VQE5sAfNae" role="3cqZAp">
                        <node concept="2OqwBi" id="3VQE5sAfOCo" role="3clFbG">
                          <node concept="1eOMI4" id="3VQE5sAfOxG" role="2Oq$k0">
                            <node concept="10QFUN" id="3VQE5sAfOxH" role="1eOMHV">
                              <node concept="37vLTw" id="3VQE5sAg0bT" role="10QFUP">
                                <ref role="3cqZAo" node="7vxxPMIkGSf" resolve="g" />
                              </node>
                              <node concept="3uibUv" id="3VQE5sAfOB8" role="10QFUM">
                                <ref role="3uigEE" to="1t7x:~Graphics2D" resolve="Graphics2D" />
                              </node>
                            </node>
                          </node>
                          <node concept="liA8E" id="3VQE5sAfPYp" role="2OqNvi">
                            <ref role="37wK5l" to="1t7x:~Graphics2D.fill(java.awt.Shape):void" resolve="fill" />
                            <node concept="37vLTw" id="3VQE5sAfVf1" role="37wK5m">
                              <ref role="3cqZAo" node="3VQE5sAfwXR" resolve="s" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2tJIrI" id="7vxxPMInUkL" role="jymVt" />
                  <node concept="2tJIrI" id="7vxxPMInUF9" role="jymVt" />
                  <node concept="3clFb_" id="7vxxPMInV45" role="jymVt">
                    <property role="1EzhhJ" value="false" />
                    <property role="TrG5h" value="getHeight" />
                    <property role="DiZV1" value="false" />
                    <property role="IEkAT" value="false" />
                    <node concept="3Tm1VV" id="7vxxPMInV46" role="1B3o_S" />
                    <node concept="10Oyi0" id="7vxxPMInV48" role="3clF45" />
                    <node concept="3clFbS" id="7vxxPMInV4a" role="3clF47">
                      <node concept="3cpWs6" id="7vxxPMInVyb" role="3cqZAp">
                        <node concept="3cpWs3" id="7vxxPMIpp3H" role="3cqZAk">
                          <node concept="2OqwBi" id="7vxxPMIo0V5" role="3uHU7B">
                            <node concept="2OqwBi" id="7vxxPMInVRF" role="2Oq$k0">
                              <node concept="Xjq3P" id="7vxxPMInVA9" role="2Oq$k0" />
                              <node concept="liA8E" id="7vxxPMInYm7" role="2OqNvi">
                                <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getParent():jetbrains.mps.nodeEditor.cells.EditorCell_Collection" resolve="getParent" />
                              </node>
                            </node>
                            <node concept="liA8E" id="7vxxPMIo3k$" role="2OqNvi">
                              <ref role="37wK5l" to="jsgz:~EditorCell_Basic.getHeight():int" resolve="getHeight" />
                            </node>
                          </node>
                          <node concept="3cmrfG" id="7vxxPMIq2ti" role="3uHU7w">
                            <property role="3cmrfH" value="6" />
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="2AHcQZ" id="7vxxPMInV4b" role="2AJF6D">
                      <ref role="2AI5Lk" to="e2lb:~Override" resolve="Override" />
                    </node>
                  </node>
                  <node concept="37vLTw" id="7vxxPMIkGXi" role="37wK5m">
                    <ref role="3cqZAo" node="7vxxPMIl1bj" resolve="context" />
                  </node>
                  <node concept="2OqwBi" id="7vxxPMIkH4F" role="37wK5m">
                    <node concept="Xjq3P" id="7vxxPMIkH0Q" role="2Oq$k0">
                      <ref role="1HBi2w" node="7vxxPMIj1UZ" resolve="BracketedCellProvider" />
                    </node>
                    <node concept="2OwXpG" id="7vxxPMIkH_x" role="2OqNvi">
                      <ref role="2Oxat5" node="7vxxPMIjghL" resolve="myNode" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7vxxPMIkLRj" role="3cqZAp" />
        <node concept="3cpWs6" id="7vxxPMIl2o0" role="3cqZAp">
          <node concept="37vLTw" id="7vxxPMIkMSs" role="3cqZAk">
            <ref role="3cqZAo" node="7vxxPMIjzC0" resolve="cell" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

