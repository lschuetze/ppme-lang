<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:7b9fdad0-3ae6-446d-a285-66665c0f952e(de.ppme.statements.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
    </language>
    <language id="3a13115c-633c-4c5c-bbcc-75c4219e9555" name="jetbrains.mps.lang.quotation">
      <concept id="5455284157993863837" name="jetbrains.mps.lang.quotation.structure.NodeBuilder" flags="nn" index="2pJPEk">
        <child id="5455284157993863838" name="quotedNode" index="2pJPEn" />
      </concept>
      <concept id="5455284157993863840" name="jetbrains.mps.lang.quotation.structure.NodeBuilderNode" flags="nn" index="2pJPED">
        <reference id="5455284157993910961" name="concept" index="2pJxaS" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1185788614172" name="jetbrains.mps.lang.typesystem.structure.NormalTypeClause" flags="ng" index="mw_s8">
        <child id="1185788644032" name="normalType" index="mwGJk" />
      </concept>
      <concept id="1185805035213" name="jetbrains.mps.lang.typesystem.structure.WhenConcreteStatement" flags="nn" index="nvevp">
        <child id="1185805047793" name="body" index="nvhr_" />
        <child id="1185805056450" name="argument" index="nvjzm" />
        <child id="1205761991995" name="argumentRepresentator" index="2X0Ygz" />
      </concept>
      <concept id="1205762105978" name="jetbrains.mps.lang.typesystem.structure.WhenConcreteVariableDeclaration" flags="ng" index="2X1qdy" />
      <concept id="1205762656241" name="jetbrains.mps.lang.typesystem.structure.WhenConcreteVariableReference" flags="nn" index="2X3wrD">
        <reference id="1205762683928" name="whenConcreteVar" index="2X3Bk0" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <property id="1195213689297" name="overrides" index="18ip37" />
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174643105530" name="jetbrains.mps.lang.typesystem.structure.InferenceRule" flags="ig" index="1YbPZF" />
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
      <concept id="1174657487114" name="jetbrains.mps.lang.typesystem.structure.TypeOfExpression" flags="nn" index="1Z2H0r">
        <child id="1174657509053" name="term" index="1Z2MuG" />
      </concept>
      <concept id="1174658326157" name="jetbrains.mps.lang.typesystem.structure.CreateEquationStatement" flags="nn" index="1Z5TYs" />
      <concept id="1174660718586" name="jetbrains.mps.lang.typesystem.structure.AbstractEquationStatement" flags="nn" index="1Zf1VF">
        <property id="1206359757216" name="checkOnly" index="3wDh2S" />
        <child id="1174660783413" name="leftExpression" index="1ZfhK$" />
        <child id="1174660783414" name="rightExpression" index="1ZfhKB" />
      </concept>
      <concept id="1174663118805" name="jetbrains.mps.lang.typesystem.structure.CreateLessThanInequationStatement" flags="nn" index="1ZobV4" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1YbPZF" id="3P8w2Hn265Y">
    <property role="TrG5h" value="typeof_ExpressionStatement" />
    <node concept="3clFbS" id="3P8w2Hn265Z" role="18ibNy">
      <node concept="1Z5TYs" id="3P8w2Hn267t" role="3cqZAp">
        <node concept="mw_s8" id="3P8w2Hn267T" role="1ZfhKB">
          <node concept="1Z2H0r" id="3P8w2Hn267P" role="mwGJk">
            <node concept="2OqwBi" id="3P8w2Hn269W" role="1Z2MuG">
              <node concept="1YBJjd" id="3P8w2Hn268d" role="2Oq$k0">
                <ref role="1YBMHb" node="3P8w2Hn2661" resolve="es" />
              </node>
              <node concept="3TrEf2" id="3P8w2Hn26sJ" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:5l83jlMhh0F" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3P8w2Hn267w" role="1ZfhK$">
          <node concept="1Z2H0r" id="3P8w2Hn2668" role="mwGJk">
            <node concept="1YBJjd" id="3P8w2Hn266r" role="1Z2MuG">
              <ref role="1YBMHb" node="3P8w2Hn2661" resolve="es" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3P8w2Hn2661" role="1YuTPh">
      <property role="TrG5h" value="es" />
      <ref role="1YaFvo" to="c9eo:5l83jlMhh0E" resolve="ExpressionStatement" />
    </node>
  </node>
  <node concept="1YbPZF" id="3P8w2Hn26D9">
    <property role="TrG5h" value="typeof_VariableReference" />
    <property role="3GE5qa" value="variables" />
    <property role="18ip37" value="true" />
    <node concept="3clFbS" id="3P8w2Hn26Da" role="18ibNy">
      <node concept="1Z5TYs" id="3P8w2Hn26ER" role="3cqZAp">
        <node concept="mw_s8" id="3P8w2Hn26Fj" role="1ZfhKB">
          <node concept="1Z2H0r" id="3P8w2Hn26Ff" role="mwGJk">
            <node concept="2OqwBi" id="4oRHscQ$4uG" role="1Z2MuG">
              <node concept="1YBJjd" id="4oRHscQ$3h1" role="2Oq$k0">
                <ref role="1YBMHb" node="3P8w2Hn26Dc" resolve="variableReference" />
              </node>
              <node concept="3TrEf2" id="4oRHscQ$eCZ" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:5l83jlMivj4" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3P8w2Hn26EU" role="1ZfhK$">
          <node concept="1Z2H0r" id="3P8w2Hn26Dg" role="mwGJk">
            <node concept="1YBJjd" id="3P8w2Hn26DJ" role="1Z2MuG">
              <ref role="1YBMHb" node="3P8w2Hn26Dc" resolve="variableReference" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3P8w2Hn26Dc" role="1YuTPh">
      <property role="TrG5h" value="variableReference" />
      <ref role="1YaFvo" to="c9eo:5l83jlMivi_" resolve="VariableReference" />
    </node>
  </node>
  <node concept="1YbPZF" id="3P8w2Hn26Ym">
    <property role="TrG5h" value="typeof_VariableDeclaration" />
    <property role="3GE5qa" value="variables" />
    <property role="18ip37" value="true" />
    <node concept="3clFbS" id="3P8w2Hn26Yn" role="18ibNy">
      <node concept="nvevp" id="rmKMEZimQX" role="3cqZAp">
        <node concept="3clFbS" id="rmKMEZimQZ" role="nvhr_">
          <node concept="3clFbJ" id="rmKMEZ9c8p" role="3cqZAp">
            <node concept="3clFbS" id="rmKMEZ9c8r" role="3clFbx">
              <node concept="1ZobV4" id="3P8w2Hn278X" role="3cqZAp">
                <property role="3wDh2S" value="true" />
                <node concept="mw_s8" id="3P8w2Hn27vz" role="1ZfhKB">
                  <node concept="1Z2H0r" id="3P8w2Hn27vv" role="mwGJk">
                    <node concept="2OqwBi" id="3P8w2Hn27zk" role="1Z2MuG">
                      <node concept="1YBJjd" id="3P8w2Hn27vR" role="2Oq$k0">
                        <ref role="1YBMHb" node="3P8w2Hn26Yp" resolve="variableDeclaration" />
                      </node>
                      <node concept="3TrEf2" id="3P8w2Hn27P_" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:6fgLCPsByeL" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="mw_s8" id="3P8w2Hn279a" role="1ZfhK$">
                  <node concept="1Z2H0r" id="3P8w2Hn2796" role="mwGJk">
                    <node concept="2OqwBi" id="3P8w2Hn27cV" role="1Z2MuG">
                      <node concept="1YBJjd" id="3P8w2Hn279u" role="2Oq$k0">
                        <ref role="1YBMHb" node="3P8w2Hn26Yp" resolve="variableDeclaration" />
                      </node>
                      <node concept="3TrEf2" id="3P8w2Hn27tN" role="2OqNvi">
                        <ref role="3Tt5mk" to="c9eo:5l83jlMhFC2" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="rmKMEZ9dgf" role="3clFbw">
              <node concept="2OqwBi" id="rmKMEZ9ccx" role="2Oq$k0">
                <node concept="1YBJjd" id="rmKMEZ9c8V" role="2Oq$k0">
                  <ref role="1YBMHb" node="3P8w2Hn26Yp" resolve="variableDeclaration" />
                </node>
                <node concept="3TrEf2" id="rmKMEZ9cYm" role="2OqNvi">
                  <ref role="3Tt5mk" to="c9eo:5l83jlMhFC2" />
                </node>
              </node>
              <node concept="3x8VRR" id="rmKMEZ9dvX" role="2OqNvi" />
            </node>
          </node>
          <node concept="1Z5TYs" id="3P8w2Hn2aUr" role="3cqZAp">
            <node concept="mw_s8" id="rmKMEZin$y" role="1ZfhKB">
              <node concept="2X3wrD" id="rmKMEZin$w" role="mwGJk">
                <ref role="2X3Bk0" node="rmKMEZimR3" resolve="varType" />
              </node>
            </node>
            <node concept="mw_s8" id="3P8w2Hn2aUu" role="1ZfhK$">
              <node concept="1Z2H0r" id="3P8w2Hn2aRT" role="mwGJk">
                <node concept="1YBJjd" id="3P8w2Hn2aSK" role="1Z2MuG">
                  <ref role="1YBMHb" node="3P8w2Hn26Yp" resolve="variableDeclaration" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="1Z2H0r" id="rmKMEZimSd" role="nvjzm">
          <node concept="2OqwBi" id="rmKMEZimW2" role="1Z2MuG">
            <node concept="1YBJjd" id="rmKMEZimSD" role="2Oq$k0">
              <ref role="1YBMHb" node="3P8w2Hn26Yp" resolve="variableDeclaration" />
            </node>
            <node concept="3TrEf2" id="rmKMEZinej" role="2OqNvi">
              <ref role="3Tt5mk" to="pfd6:6fgLCPsByeL" />
            </node>
          </node>
        </node>
        <node concept="2X1qdy" id="rmKMEZimR3" role="2X0Ygz">
          <property role="TrG5h" value="varType" />
          <node concept="2jxLKc" id="rmKMEZimR4" role="1tU5fm" />
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3P8w2Hn26Yp" role="1YuTPh">
      <property role="TrG5h" value="variableDeclaration" />
      <ref role="1YaFvo" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
    </node>
  </node>
  <node concept="1YbPZF" id="3yJ4dri108t">
    <property role="TrG5h" value="typeof_IfStatement" />
    <node concept="3clFbS" id="3yJ4dri108u" role="18ibNy">
      <node concept="1ZobV4" id="3yJ4dri10hr" role="3cqZAp">
        <node concept="mw_s8" id="3yJ4dri10B2" role="1ZfhKB">
          <node concept="2pJPEk" id="3yJ4dri16HM" role="mwGJk">
            <node concept="2pJPED" id="3yJ4dri16HY" role="2pJPEn">
              <ref role="2pJxaS" to="pfd6:6fgLCPsBxs4" resolve="BooleanType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3yJ4dri10h_" role="1ZfhK$">
          <node concept="1Z2H0r" id="3yJ4dri10hx" role="mwGJk">
            <node concept="2OqwBi" id="3yJ4dri10jy" role="1Z2MuG">
              <node concept="1YBJjd" id="3yJ4dri10hQ" role="2Oq$k0">
                <ref role="1YBMHb" node="3yJ4dri108w" resolve="ifStatement" />
              </node>
              <node concept="3TrEf2" id="3yJ4dri10_V" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:3yJ4dri0HIg" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3yJ4dri108w" role="1YuTPh">
      <property role="TrG5h" value="ifStatement" />
      <ref role="1YaFvo" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
    </node>
  </node>
  <node concept="1YbPZF" id="3yJ4dri1d9h">
    <property role="TrG5h" value="typeof_ElseIfClause" />
    <node concept="3clFbS" id="3yJ4dri1d9i" role="18ibNy">
      <node concept="1ZobV4" id="3yJ4dri1d9o" role="3cqZAp">
        <node concept="mw_s8" id="3yJ4dri1djS" role="1ZfhKB">
          <node concept="2pJPEk" id="3yJ4dri1djO" role="mwGJk">
            <node concept="2pJPED" id="3yJ4dri1dk3" role="2pJPEn">
              <ref role="2pJxaS" to="pfd6:6fgLCPsBxs4" resolve="BooleanType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="3yJ4dri1d9y" role="1ZfhK$">
          <node concept="1Z2H0r" id="3yJ4dri1d9u" role="mwGJk">
            <node concept="2OqwBi" id="3yJ4dri1db9" role="1Z2MuG">
              <node concept="1YBJjd" id="3yJ4dri1d9N" role="2Oq$k0">
                <ref role="1YBMHb" node="3yJ4dri1d9k" resolve="elseIfClause" />
              </node>
              <node concept="3TrEf2" id="3yJ4dri1diQ" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:3yJ4dri1cHH" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3yJ4dri1d9k" role="1YuTPh">
      <property role="TrG5h" value="elseIfClause" />
      <ref role="1YaFvo" to="c9eo:3yJ4dri1cHG" resolve="ElseIfClause" />
    </node>
  </node>
  <node concept="1YbPZF" id="6JTxo0b1D1_">
    <property role="TrG5h" value="typeof_ForEachStatement" />
    <property role="3GE5qa" value="loops" />
    <node concept="3clFbS" id="6JTxo0b1D1A" role="18ibNy" />
    <node concept="1YaCAy" id="6JTxo0b1D1C" role="1YuTPh">
      <property role="TrG5h" value="forEachStatement" />
      <ref role="1YaFvo" to="c9eo:6JTxo0b0Q4k" resolve="ForEachStatement" />
    </node>
  </node>
  <node concept="1YbPZF" id="26Us85Xyzzv">
    <property role="TrG5h" value="typeof_VariableDeclarationStatement" />
    <property role="3GE5qa" value="variables" />
    <node concept="3clFbS" id="26Us85Xyzzw" role="18ibNy">
      <node concept="1Z5TYs" id="26Us85XyL4u" role="3cqZAp">
        <node concept="mw_s8" id="26Us85XyL4M" role="1ZfhKB">
          <node concept="1Z2H0r" id="26Us85XyL4I" role="mwGJk">
            <node concept="2OqwBi" id="26Us85XyL78" role="1Z2MuG">
              <node concept="1YBJjd" id="26Us85XyL53" role="2Oq$k0">
                <ref role="1YBMHb" node="26Us85Xyzzy" resolve="declStmnt" />
              </node>
              <node concept="3TrEf2" id="26Us85XyVfs" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:5o9jvTMcVMR" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="26Us85XyL4x" role="1ZfhK$">
          <node concept="1Z2H0r" id="26Us85XyKIt" role="mwGJk">
            <node concept="1YBJjd" id="26Us85XyKIT" role="1Z2MuG">
              <ref role="1YBMHb" node="26Us85Xyzzy" resolve="declStmnt" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="26Us85Xyzzy" role="1YuTPh">
      <property role="TrG5h" value="declStmnt" />
      <ref role="1YaFvo" to="c9eo:5o9jvTMcVMM" resolve="VariableDeclarationStatement" />
    </node>
  </node>
  <node concept="1YbPZF" id="1RtLc$XKVzz">
    <property role="TrG5h" value="typeof_PrintStatement" />
    <node concept="3clFbS" id="1RtLc$XKVz$" role="18ibNy">
      <node concept="1ZobV4" id="1RtLc$XKV_0" role="3cqZAp">
        <property role="3wDh2S" value="true" />
        <node concept="mw_s8" id="1RtLc$XKVNX" role="1ZfhKB">
          <node concept="2pJPEk" id="1RtLc$XKVNT" role="mwGJk">
            <node concept="2pJPED" id="1RtLc$XKVO8" role="2pJPEn">
              <ref role="2pJxaS" to="pfd6:6fgLCPsBxeW" resolve="IntegerType" />
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="1RtLc$XKV_a" role="1ZfhK$">
          <node concept="1Z2H0r" id="1RtLc$XKV_6" role="mwGJk">
            <node concept="2OqwBi" id="1RtLc$XKVBw" role="1Z2MuG">
              <node concept="1YBJjd" id="1RtLc$XKV_r" role="2Oq$k0">
                <ref role="1YBMHb" node="1RtLc$XKVzA" resolve="printStatement" />
              </node>
              <node concept="3TrEf2" id="1RtLc$XKVMF" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:1RtLc$XKLYm" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="1RtLc$XKVzA" role="1YuTPh">
      <property role="TrG5h" value="printStatement" />
      <ref role="1YaFvo" to="c9eo:3yJ4dri0bx9" resolve="PrintStatement" />
    </node>
  </node>
  <node concept="1YbPZF" id="hRSdiNcK2l">
    <property role="TrG5h" value="typeof_LocalVariableDeclarationStatement" />
    <property role="3GE5qa" value="variables" />
    <node concept="3clFbS" id="hRSdiNcK2m" role="18ibNy">
      <node concept="1Z5TYs" id="hRSdiNcK5w" role="3cqZAp">
        <node concept="mw_s8" id="hRSdiNcK5O" role="1ZfhKB">
          <node concept="1Z2H0r" id="hRSdiNcK5K" role="mwGJk">
            <node concept="2OqwBi" id="hRSdiNcK84" role="1Z2MuG">
              <node concept="1YBJjd" id="hRSdiNcK65" role="2Oq$k0">
                <ref role="1YBMHb" node="hRSdiNcK2o" resolve="localVariableDeclarationStatement" />
              </node>
              <node concept="3TrEf2" id="hRSdiNcKjv" role="2OqNvi">
                <ref role="3Tt5mk" to="c9eo:hRSdiNcINU" />
              </node>
            </node>
          </node>
        </node>
        <node concept="mw_s8" id="hRSdiNcK5z" role="1ZfhK$">
          <node concept="1Z2H0r" id="hRSdiNcK3X" role="mwGJk">
            <node concept="1YBJjd" id="hRSdiNcK4p" role="1Z2MuG">
              <ref role="1YBMHb" node="hRSdiNcK2o" resolve="localVariableDeclarationStatement" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="hRSdiNcK2o" role="1YuTPh">
      <property role="TrG5h" value="localVariableDeclarationStatement" />
      <ref role="1YaFvo" to="c9eo:hRSdiNcI$6" resolve="LocalVariableDeclarationStatement" />
    </node>
  </node>
</model>

