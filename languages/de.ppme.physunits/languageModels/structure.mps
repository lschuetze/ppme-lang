<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:505a4475-a865-4dba-9f1a-b7c0cce39ec5(de.ppme.physunits.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="6054523464627964745" name="jetbrains.mps.lang.structure.structure.AttributeInfo_AttributedConcept" flags="ng" index="trNpa">
        <reference id="6054523464627965081" name="concept" index="trN6q" />
      </concept>
      <concept id="2992811758677295509" name="jetbrains.mps.lang.structure.structure.AttributeInfo" flags="ng" index="M6xJ_">
        <property id="7588428831955550663" name="role" index="Hh88m" />
        <child id="7588428831947959310" name="attributed" index="EQaZv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="PlHQZ" id="298Hrzl_3BW">
    <property role="TrG5h" value="IPhysicalUnit" />
    <property role="3GE5qa" value="units" />
    <node concept="PrWs8" id="298Hrzl_eEW" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="298Hrzl_feV">
    <property role="TrG5h" value="Exponent" />
    <property role="3GE5qa" value="units" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="298Hrzl_fu0" role="1TKVEl">
      <property role="TrG5h" value="value" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="298HrzlAXFd">
    <property role="TrG5h" value="PhysicalUnit" />
    <property role="3GE5qa" value="units" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="298HrzlAXFk" role="PzmwI">
      <ref role="PrY4T" node="298Hrzl_3BW" resolve="IPhysicalUnit" />
    </node>
    <node concept="PrWs8" id="298HrzlAXFp" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="298HrzlAXFt" role="1TKVEl">
      <property role="TrG5h" value="desc" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyj" id="298HrzlBa2B" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="spec" />
      <ref role="20lvS9" node="298HrzlAXFD" resolve="PhysicalUnitSpecification" />
    </node>
  </node>
  <node concept="1TIwiD" id="298HrzlAXFD">
    <property role="TrG5h" value="PhysicalUnitSpecification" />
    <property role="3GE5qa" value="units" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="298HrzlB1H4" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="component" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="298HrzlAXFM" resolve="PhysicalUnitRef" />
    </node>
  </node>
  <node concept="1TIwiD" id="298HrzlAXFM">
    <property role="TrG5h" value="PhysicalUnitRef" />
    <property role="3GE5qa" value="units" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="298HrzlAXFT" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="exponent" />
      <ref role="20lvS9" node="298Hrzl_feV" resolve="Exponent" />
    </node>
    <node concept="1TJgyj" id="298HrzlAXFV" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="decl" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="298Hrzl_3BW" resolve="IPhysicalUnit" />
    </node>
  </node>
  <node concept="1TIwiD" id="298HrzlBa4F">
    <property role="TrG5h" value="PhysicalUnitDeclarations" />
    <property role="19KtqR" value="true" />
    <property role="3GE5qa" value="units" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="298HrzlBa4G" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="298HrzlBa4I" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="units" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="298HrzlAXFd" resolve="PhysicalUnit" />
    </node>
  </node>
  <node concept="1TIwiD" id="298HrzlCobB">
    <property role="TrG5h" value="AnnotatedType" />
    <property role="3GE5qa" value="annotations" />
    <ref role="1TJDcQ" to="pfd6:6fgLCPsAWoz" resolve="Type" />
    <node concept="1TJgyj" id="298HrzlF7GJ" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="primtype" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:6fgLCPsAWoz" resolve="Type" />
    </node>
    <node concept="1TJgyj" id="298HrzlCojK" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="spec" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="298HrzlAXFD" resolve="PhysicalUnitSpecification" />
    </node>
  </node>
  <node concept="1TIwiD" id="298HrzlDAuk">
    <property role="TrG5h" value="AnnotatedExpression" />
    <property role="3GE5qa" value="annotations" />
    <ref role="1TJDcQ" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    <node concept="1TJgyj" id="298HrzlFgxM" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="expr" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="298HrzlDAvl" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="spec" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="298HrzlAXFD" resolve="PhysicalUnitSpecification" />
    </node>
  </node>
  <node concept="1TIwiD" id="2Xgo$egBfw$">
    <property role="TrG5h" value="MetaPhysicalUnit" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="2Xgo$egBfwZ" role="PzmwI">
      <ref role="PrY4T" node="298Hrzl_3BW" resolve="IPhysicalUnit" />
    </node>
    <node concept="PrWs8" id="2Xgo$egBfx4" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="2Xgo$egFsG5">
    <property role="TrG5h" value="InferredSubstitution" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2Xgo$egFsVa" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="physUnit" />
      <ref role="20lvS9" node="298HrzlAXFD" resolve="PhysicalUnitSpecification" />
    </node>
    <node concept="1TJgyj" id="2Xgo$egFsVc" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="metaUnit" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2Xgo$egBfw$" resolve="MetaPhysicalUnit" />
    </node>
  </node>
  <node concept="1TIwiD" id="2Xgo$egFvsi">
    <property role="TrG5h" value="InferredAttribute" />
    <ref role="1TJDcQ" to="tpck:2ULFgo8_XDk" resolve="NodeAttribute" />
    <node concept="M6xJ_" id="2Xgo$egFvsj" role="lGtFl">
      <property role="Hh88m" value="inferred" />
      <node concept="trNpa" id="2Xgo$egFvsl" role="EQaZv">
        <ref role="trN6q" node="298HrzlCobB" resolve="AnnotatedType" />
      </node>
    </node>
    <node concept="1TJgyj" id="2Xgo$egFvsn" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="substitution" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="2Xgo$egFsG5" resolve="InferredSubstitution" />
    </node>
  </node>
  <node concept="1TIwiD" id="3hzmVlLDNvn">
    <property role="3GE5qa" value="annotations" />
    <property role="TrG5h" value="AbstractAnnotatedType" />
    <property role="34LRSv" value="annotated" />
    <ref role="1TJDcQ" to="pfd6:6fgLCPsAWoz" resolve="Type" />
  </node>
</model>

