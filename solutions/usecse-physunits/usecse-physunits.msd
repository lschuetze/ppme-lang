<?xml version="1.0" encoding="UTF-8"?>
<solution name="usecases.physunits" uuid="06e8c6a3-5645-41df-9e85-e458f0cc95bc" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <dependencies>
    <dependency reexport="false">76a86d18-59ae-4f16-b531-8543c06a850b(de.ppme.plugins)</dependency>
  </dependencies>
  <usedLanguages>
    <usedLanguage>cd53cec3-9093-4895-940d-de1b7abe9936(de.ppme.physunits)</usedLanguage>
    <usedLanguage>63e1a497-af15-4608-bf36-c0aa0aaf901b(de.ppme.core)</usedLanguage>
    <usedLanguage>7d7a484c-83ab-4a7c-b9f3-8672c02bd0f6(de.ppme.statements)</usedLanguage>
    <usedLanguage>f7af8208-82a7-4aab-9829-b32680223c52(de.ppme.ctrl)</usedLanguage>
    <usedLanguage>58df54a5-af81-48d5-bb5c-9db2412768a4(de.ppme.expressions)</usedLanguage>
    <usedLanguage>a206eff4-e667-4146-8006-8cce4ef80954(de.ppme.modules)</usedLanguage>
    <usedLanguage>ad43ad83-7356-4c72-888b-881fea736282(de.ppme.base)</usedLanguage>
  </usedLanguages>
  <languageVersions>
    <language id="ad43ad83-7356-4c72-888b-881fea736282" fqName="de.ppme.base" version="0" />
    <language id="63e1a497-af15-4608-bf36-c0aa0aaf901b" fqName="de.ppme.core" version="-1" />
    <language id="f7af8208-82a7-4aab-9829-b32680223c52" fqName="de.ppme.ctrl" version="0" />
    <language id="58df54a5-af81-48d5-bb5c-9db2412768a4" fqName="de.ppme.expressions" version="0" />
    <language id="a206eff4-e667-4146-8006-8cce4ef80954" fqName="de.ppme.modules" version="0" />
    <language id="cd53cec3-9093-4895-940d-de1b7abe9936" fqName="de.ppme.physunits" version="0" />
    <language id="7d7a484c-83ab-4a7c-b9f3-8672c02bd0f6" fqName="de.ppme.statements" version="0" />
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" fqName="jetbrains.mps.baseLanguage" version="1" />
    <language id="ed6d7656-532c-4bc2-81d1-af945aeb8280" fqName="jetbrains.mps.baseLanguage.blTypes" version="0" />
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" fqName="jetbrains.mps.lang.core" version="1" />
    <language id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" fqName="jetbrains.mps.lang.traceable" version="0" />
  </languageVersions>
</solution>

